package com.pizzacar.tomio.pizzacar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pizzacar.tomio.pizzacar.Data.CustomerData;
import com.pizzacar.tomio.pizzacar.Data.UserSharedPreferences;

public class AddressInputActivity extends Activity {
    private String fullAddress;
    private boolean isAparment;
    private CustomerData dataHelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.pizzacar.tomio.pizzacar.R.layout.activity_address_input);

        TextView signIn = (TextView) findViewById(com.pizzacar.tomio.pizzacar.R.id.text_sign_in_address);
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(AddressInputActivity.this,SignInActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);


            }
        });



        final EditText addressField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.field_address);
        final EditText apartmentField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.field_apartment);
        final CheckBox isApartmentField = (CheckBox) findViewById(com.pizzacar.tomio.pizzacar.R.id.field_is_aparment);

        isAparment = false;

        isApartmentField.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if(isChecked)
                {
                    apartmentField.setVisibility(View.VISIBLE);

                }

                isAparment = isChecked;
            }
        });
        Button signUpButton = (Button) findViewById(com.pizzacar.tomio.pizzacar.R.id.button_sign_up);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                String address = addressField.getText().toString();
                if(isAparment)
                {
                    String apt = apartmentField.getText().toString();
                    fullAddress = address + " Apt. "+apt;
                }
                else
                    fullAddress = address;

                UpdateAddressTask task = new UpdateAddressTask();
                task.execute(fullAddress);


            }
        });


        Aesthetic aesthetic = new Aesthetic();
        aesthetic.setBarColor(this);
    }

    public class UpdateAddressTask extends AsyncTask<String,Void,String>
    {
        private String TAG  = UpdateAddressTask.class.getSimpleName();
        private ProgressDialog dialog = new ProgressDialog(AddressInputActivity.this);

        @Override
        protected void onPreExecute()
        {
            //this.dialog.setMessage("Please wait");
            this.dialog.show();
        }
        @Override
        protected String doInBackground(String... params)
        {


             dataHelper = new CustomerData(AddressInputActivity.this);
             return dataHelper.updateAddress(params[0]);
        }

        @Override
        public void onPostExecute(String validity)
        {

            dialog.dismiss();

            if(validity.equals("NOT_FOUND"))
            {
                    Toast.makeText(AddressInputActivity.this,dataHelper.getSignUpErrorMessage(),Toast.LENGTH_LONG).show();
            }
            else if(validity.equals("TOO_FAR"))
            {
                startActivity(new Intent(AddressInputActivity.this,AddressTooFar.class));
            }
            else
            {
                UserSharedPreferences.setAddressIsValid(AddressInputActivity.this,true);
                Intent intent = new Intent(AddressInputActivity.this, Home.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);

            }

        }
    }


}
