package com.pizzacar.tomio.pizzacar;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;

/**
 * Created by tomio on 7/25/2016.
 */
public class FragmentPageManager extends FragmentPagerAdapter {
    final int PAGE_COUNT = 2;

    private Context context;
    private SparseArray<Fragment> registeredFragments = new SparseArray<>();
    private String TAG = FragmentPageManager.class.getSimpleName();
    public FragmentPageManager(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position)
    {



        if (position == 0)
            return new OrderFragment();
        else
            return new OrderInfoFragment();
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        Log.v(TAG,"POSITION NOW "+getSize());
        return fragment;
    }

    public Fragment getRegisteredFragment(int position)
    {
        return registeredFragments.get(position);


    }

    public int getSize(){ return registeredFragments.size();}



}