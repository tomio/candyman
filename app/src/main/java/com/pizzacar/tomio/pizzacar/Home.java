package com.pizzacar.tomio.pizzacar;

import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.pizzacar.tomio.pizzacar.Data.CustomerData;
import com.pizzacar.tomio.pizzacar.Data.UserSharedPreferences;

public class Home extends AppCompatActivity {

    private final String TAG_ORDER_INFO = "ORDER_INFO_FRAG";
    private final String TAG_ORDER = "ORDER_FRAG";
    public static final String SENT_TOKEN = "send ds";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private static final String TAG = Home.class.getSimpleName();
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        private final Handler handler = new Handler();


        @Override
        public void onReceive(final Context context, final Intent intent) {
            handler.post(new Runnable() {

                @Override
                public void run() {

                    OrderInfoFragment frag = (OrderInfoFragment) getFragmentManager().findFragmentByTag(TAG_ORDER_INFO);

                    if (frag != null)
                    {
                        frag.setTimeText();
                    }
                }
            });

        }
    };


    private FragmentPageManager adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(com.pizzacar.tomio.pizzacar.R.layout.activity_home);



        Toolbar toolbar = (Toolbar) findViewById(com.pizzacar.tomio.pizzacar.R.id.toolbar);
        setSupportActionBar(toolbar);

        registerReceiver(broadcastReceiver, new IntentFilter("GET_ME"));
        new CheckOrderExistanceTask().execute();
        Aesthetic.setBarColor(Home.this);
    }

    @Override
    public void onResume() {
        super.onResume();

        if(!UserSharedPreferences.checkAddressIsValid(this))
            startActivity(new Intent(Home.this,AddressInputActivity.class));


        if(isGooglePlayAvaliable())
        {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            boolean sentToken = sharedPreferences.getBoolean(SENT_TOKEN,false);
            if(!sentToken)
                startService(new Intent(this,RegistrationIntentService.class));

        }

    }

    private boolean isGooglePlayAvaliable()
    {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if(resultCode != ConnectionResult.SUCCESS)
        {
            if(googleApiAvailability.isUserResolvableError(resultCode))
            {
                googleApiAvailability.getErrorDialog(this,resultCode,PLAY_SERVICES_RESOLUTION_REQUEST).show();

            }

            return false;
        }

        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.pizzacar.tomio.pizzacar.R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        int id = item.getItemId();

        if (id == R.id.action_sign_out) {

            unregisterReceiver(broadcastReceiver);

            PendingIntent sender = PendingIntent.getBroadcast(this, 0, new Intent(this,AlarmReceiver.class), PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarmMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmMgr.cancel(sender);

            UserSharedPreferences.deletePreferences(Home.this);
            Intent intent = new Intent(Home.this,SignInActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            return true;
        }

        if (id == R.id.action_settings)
        {
            startActivity(new Intent(Home.this,AccountInfoActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    public void setFragment(String fragName)
    {
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        Fragment fragment;
        Fragment oldFrag = null;
        String tag;
        if (fragName.equals("order")) {
            oldFrag = (OrderInfoFragment) manager.findFragmentByTag(TAG_ORDER_INFO);
            fragment = new OrderFragment();
            tag = TAG_ORDER;
        } else {

            oldFrag = (OrderFragment) manager.findFragmentByTag(TAG_ORDER_INFO);
            fragment = new OrderInfoFragment();
            tag = TAG_ORDER_INFO;
        }

        if (oldFrag != null)
            transaction.remove(oldFrag);

        transaction.add(R.id.container_home, fragment,tag );
        transaction.commitAllowingStateLoss();

    }


    public class CheckOrderExistanceTask extends AsyncTask<Void, Void, Boolean>
    {


        @Override
        protected Boolean doInBackground(Void... params)
        {
            CustomerData dataHelper = new CustomerData(Home.this);
            return dataHelper.orderExists();

        }

        @Override
        protected void onPostExecute(Boolean existance)
        {

            String fragmentName;

            if(existance)
                fragmentName = "info";
            else
                fragmentName = "order";

            setFragment(fragmentName);


        }

    }

}
