package com.pizzacar.tomio.pizzacar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pizzacar.tomio.pizzacar.Driver.DriverHome;
import com.pizzacar.tomio.pizzacar.Data.CustomerData;
import com.pizzacar.tomio.pizzacar.Data.UserSharedPreferences;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ActivitySignUp extends Activity {
    private EditText emailField;
    private EditText passwordField;
    private TextView signInText;
    private String email;
    private String password;

    private CustomerData dataHelper;

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_sign_up);

        long userId = UserSharedPreferences.getUserId(ActivitySignUp.this);
        if (userId> 0)
        {

            if(userId == 3)
            {
                Intent intent = new Intent(ActivitySignUp.this, DriverHome.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
            else
            {
                Intent intent = new Intent(ActivitySignUp.this,Home.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }

        }


        signInText = (TextView) findViewById(com.pizzacar.tomio.pizzacar.R.id.text_sign_in);
        emailField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.field_email);
        passwordField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.field_password);
        Button nextButton = (Button) findViewById(com.pizzacar.tomio.pizzacar.R.id.button_next);



        signInText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivitySignUp.this,SignInActivity.class));
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = emailField.getText().toString().trim();
                String password = passwordField.getText().toString().trim();

                if(validate(email))
                {
                    if (password.length() >= 6)
                    {

                        SignUpTask task = new SignUpTask();
                        task.execute(email,password);


                    }

                    else
                    {
                        Toast.makeText(ActivitySignUp.this,"Password must be at least 6 characters",Toast.LENGTH_LONG).show();
                    }

                }

                else
                {
                    Toast.makeText(ActivitySignUp.this,"Please enter a valid email",Toast.LENGTH_LONG).show();

                }
            }
        });

            Aesthetic.setBarColor(this);

    }

    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
    }

    public class SignUpTask extends AsyncTask<String,Void,Long>
    {

        private String TAG  = SignUpTask.class.getSimpleName();

        private ProgressDialog dialog = new ProgressDialog(ActivitySignUp.this);

        @Override
        protected void onPreExecute()
        {
            //this.dialog.setMessage("Please wait");
            this.dialog.show();
        }
        @Override
        protected Long doInBackground(String... params)
        {
            email = params[0];
            password = params[1];
            dataHelper = new CustomerData(ActivitySignUp.this);
            return dataHelper.createCustomer(email,password);
        }

        @Override
        public void onPostExecute(Long userId)
        {
            dialog.dismiss();
            if(userId > 0)
            {
                UserSharedPreferences.setUserEmail(ActivitySignUp.this, email);
                UserSharedPreferences.setUserId(ActivitySignUp.this, userId);
                UserSharedPreferences.setUserPassword(ActivitySignUp.this, password);

                Intent intent = new Intent(ActivitySignUp.this,AddressInputActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
            else
            {
                Toast.makeText(ActivitySignUp.this,dataHelper.getSignUpErrorMessage(),Toast.LENGTH_LONG).show();
            }




        }
    }

    }
