package com.pizzacar.tomio.pizzacar.Data;

import android.net.Uri;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by tomio on 5/7/2016.
 */
public class LocationHelper {
    private static final String TAG = LocationHelper.class.getSimpleName();
    private static final String API_KEY = "AIzaSyCma7yOGAngxdf7ZPaKkfm22S1WFTTT7QI";
    private static final double ACCEPTED_DISTANCE = 20;

    public static String getDistance(double orginLong, double orginLat, String destination) {
        String distance = null;
        HttpURLConnection con = null;
        BufferedReader reader = null;
        String jsonString;
        try

        {


            String origin = orginLong + "," + orginLat;
            Uri uri = Uri.parse("https://maps.googleapis.com/maps/api/distancematrix/json");

            Uri.Builder builder = uri.buildUpon().appendQueryParameter("origins", origin)
                    .appendQueryParameter("destinations", destination)
                    .appendQueryParameter("key", API_KEY);
            String urlString = builder.toString();
            Log.v(TAG, "Url String: " + urlString);


            URL url = new URL(urlString);

            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();

            InputStream in = con.getInputStream();
            StringBuffer buffer = new StringBuffer();

            if (in == null) {
                return distance;
            }

            reader = new BufferedReader(new InputStreamReader(in));
            String line;

            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");

            }

            if (buffer.length() == 0) {
                return distance;

            }
            jsonString = buffer.toString();
            Log.v(TAG, "Json String : " + jsonString);

            try {

                String value = extractDistance(jsonString);
                Log.v(TAG, "Distance: " + value);
                return value;
            } catch (JSONException e) {
                Log.e(TAG, "There was a probmlem extracting from the json String " + e.getMessage(), e);
                e.printStackTrace();
            }


        } catch (IOException e) {
            Log.e("PlaceholderFragment", "Error ", e);
        } finally {
            if (con != null)
                con.disconnect();
        }

        if (reader != null) {
            try {
                reader.close();

            } catch (final IOException e) {
                Log.e(TAG, "There was a problem closing the reader ", e);

            }
        }
        return distance;

    }

    public static String extractDistance(String jsonString)
            throws JSONException {
        JSONObject directions = new JSONObject(jsonString);

        JSONObject distance = directions.getJSONArray("rows").getJSONObject(0).getJSONArray("elements").getJSONObject(0).getJSONObject("duration");

        return distance.getString("text");

    }


    public boolean isInBuilding(double distance)
    {
        if(distance <= ACCEPTED_DISTANCE)
            return true;
        else
            return false;
    }
}