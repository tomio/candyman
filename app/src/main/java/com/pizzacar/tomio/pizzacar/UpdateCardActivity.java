package com.pizzacar.tomio.pizzacar;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pizzacar.tomio.pizzacar.Data.UserSharedPreferences;
import com.pizzacar.tomio.pizzacar.Data.CustomerData;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;

import org.json.JSONException;
import org.json.JSONObject;

public class UpdateCardActivity extends AppCompatActivity {
    private EditText cardNumberField;
    private EditText cardSecurityField;
    private EditText cardExpirationMonthField;
    private EditText cardExpirationYearField;

    private TextView confirmChangeText;
    private static final String PUBLISHABLE_KEY = "pk_live_BZsprNGlg7Jnkv7eFPNxF6Lj";


    private final String TAG  = UpdateCardActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.pizzacar.tomio.pizzacar.R.layout.activity_update_card);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        cardNumberField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.field_update_card_number);
        cardExpirationMonthField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.field_update_expiration_month);
        cardExpirationYearField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.field_update_card_expiration_year);
        cardSecurityField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.field_update_card_security);
        confirmChangeText = (TextView) findViewById(com.pizzacar.tomio.pizzacar.R.id.text_update_card);

        confirmChangeText.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v)
            {
                String cardNumber = cardNumberField.getText().toString().trim();
                String cardSecurity = cardSecurityField.getText().toString().trim();
                int expirationYear = Integer.parseInt(cardExpirationYearField.getText().toString().trim());
                int expirationMonth = Integer.parseInt(cardExpirationMonthField.getText().toString().trim());


                Card card = new Card(
                        cardNumber,
                        expirationMonth,
                        expirationYear,
                        cardSecurity
                );


                if (card.validateCard()) {
                    try {
                        Stripe stripe = new Stripe(PUBLISHABLE_KEY);


                        stripe.createToken(
                                card,
                                new TokenCallback() {
                                    public void onSuccess(Token token) {

                                        // Send token to your server
                                        UpdateCardTask task = new UpdateCardTask();
                                        task.execute(token.getId());

                                     //   Toast.makeText(T.this, "Success", Toast.LENGTH_SHORT).show();

                                    }

                                    public void onError(Exception error) {
                                        // Show localized error message
                                        Toast.makeText(UpdateCardActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();

                                    }
                                }
                        );


                    } catch (AuthenticationException e)
                    {
                        Log.e(TAG,"There was a problem gettingt the stripe token ",e);
                    }


                }
                else
                {
                    new AlertDialog.Builder(UpdateCardActivity.this)
                            .setMessage("The card you entered was invalid. Please try again")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // startActivity(new Intent(PaymentMethodActivity.this, U.class));

                                }
                            })
                            .show();


                }
            }




        });





        cardNumberField.addTextChangedListener(new TextWatcher() {

            private static final char space = ' ';

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {


                // Remove spacing char
                if (s.length() > 0 && (s.length() % 5) == 0) {
                    final char c = s.charAt(s.length() - 1);
                    if (space == c) {
                        s.delete(s.length() - 1, s.length());
                    }
                }
                // Insert char where needed.
                if (s.length() > 0 && (s.length() % 5) == 0) {
                    Log.v(TAG, "I was called ");
                    char c = s.charAt(s.length() - 1);
                    // Only if its a digit where there should be a space we insert a space
                    if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3)
                    {

                        s.insert(s.length() - 1, String.valueOf(space));

                    }
                }
            }
        });

    }


    public class UpdateCardTask extends AsyncTask<String,Void,String>
    {
        private String TAG = UpdateCardTask.class.getSimpleName();
        private ProgressDialog dialog = new ProgressDialog(UpdateCardActivity.this);


        @Override
        protected void onPreExecute()
        {
            //this.dialog.setMessage("Please wait");
            this.dialog.show();
        }

        @Override
        protected String doInBackground(String... params)
        {

            CustomerData dataHelper = new CustomerData(UpdateCardActivity.this);
            String email = UserSharedPreferences.getUserEmail(UpdateCardActivity.this);
            String password = UserSharedPreferences.getUserPassword(UpdateCardActivity.this);
            String stripeToken = params[0];

            return dataHelper.updatePaymentMethod(email,password,stripeToken);
        }

        @Override
        protected void onPostExecute(String cardData)
        {
            Log.v(TAG,"The confirmed string is "+cardData);

            String cardText = null;

            try
            {
                JSONObject json =new JSONObject(cardData);

                if(json.getBoolean("success"))
                {
                    JSONObject cardJson = json.getJSONObject("customer").getJSONArray("data").getJSONObject(0);

                    cardText = cardJson.getString("brand") + " **** "+ cardJson.getString("last4");

                    Bundle passedArgs = getIntent().getExtras();

                    String email = passedArgs.getString(CustomerData.CUSTOMER_EMAIL);
                    String address = passedArgs.getString(CustomerData.CUSTOMER_ADDRESS);

                    Bundle bundle = new Bundle();
                    bundle.putString(CustomerData.CUSTOMER_EMAIL, email);
                    bundle.putString(CustomerData.CUSTOMER_PAYMENT, cardText);
                    bundle.putString(CustomerData.CUSTOMER_ADDRESS, address);

                    Intent intent = new Intent(UpdateCardActivity.this, EditAccountActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                    intent.putExtras(bundle);
                    this.dialog.dismiss();
                    startActivity(intent);
                }
                else
                {

                    this.dialog.dismiss();
                    new AlertDialog.Builder(UpdateCardActivity.this)
                            .setMessage(json.getString("message")+" Please re-enter card info or try another card.")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    //startActivity(new Intent(ReviewActivity.this, Home.class));

                                }
                            })
                            .show();

                }

            }
            catch (JSONException e)
            {
                Log.e(TAG,"There was a problem gettting the updated card last 4",e);
            }



        }

    }

}