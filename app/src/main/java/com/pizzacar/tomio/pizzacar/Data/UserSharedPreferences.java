package com.pizzacar.tomio.pizzacar.Data;
import android.content.SharedPreferences;
import android.content.Context;
import android.preference.PreferenceManager;
import android.content.SharedPreferences.Editor;

public class UserSharedPreferences {
    public static final String PREF_USER_ID = "_id";
    public static final String PREF_PASSWORD = "password";
    public static final String PREF_EMAIL = "email";
    public static final String PREF_TESTER = "tester";
    public static final String PREF_ADDRESS_VALID = "address_valid";
    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }


    public static void setUserId(Context context, long id) {
        Editor editor = getSharedPreferences(context).edit();
        editor.putLong(PREF_USER_ID, id);
        editor.commit();
    }

    public static long getUserId(Context context) {
        return getSharedPreferences(context).getLong(PREF_USER_ID, -1);

    }


    public static String getUserEmail(Context context)
    {
        return getSharedPreferences(context).getString(PREF_EMAIL, "");

    }



    public static void setUserPassword(Context context, String password)
    {
        Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_PASSWORD, password);
        editor.commit();

    }

    public static String getUserPassword(Context context)
    {
        return getSharedPreferences(context).getString(PREF_PASSWORD, "");

    }



    public static void setUserEmail(Context context, String email)
    {
        Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_EMAIL, email);
        editor.commit();

    }


    public static void setIsTester(Context context, boolean is) {
        Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(PREF_TESTER, is);
        editor.commit();
    }

    public static boolean checkIsTester(Context context) {
        return getSharedPreferences(context).getBoolean(PREF_TESTER, false);

    }

    public static void setAddressIsValid(Context context, boolean is) {
        Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(PREF_ADDRESS_VALID, is);
        editor.commit();
    }

    public static boolean checkAddressIsValid(Context context)
    {
        return getSharedPreferences(context).getBoolean(PREF_ADDRESS_VALID, false);

    }

    public static void deletePreferences(Context context)
    {
        Editor editor = getSharedPreferences(context).edit();
        editor.clear();
        editor.commit();

    }



}