package com.pizzacar.tomio.pizzacar;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.pizzacar.tomio.pizzacar.Data.CustomerData;

/**
 * Created by tomio on 9/2/2016.
 */
public class RegistrationIntentService extends IntentService
{
    private static final String TAG = RegistrationIntentService.class.getSimpleName();

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try
        {
            synchronized (TAG)
            {
                InstanceID instanceID = InstanceID.getInstance(this);
                String token = instanceID.getToken(getString(R.string.gcm_default_sender), GoogleCloudMessaging.INSTANCE_ID_SCOPE,null);
                sendRegistrationToServer(this,token);
                sharedPreferences.edit().putBoolean(Home.SENT_TOKEN,true).apply();
            }
        }
        catch (Exception e)
        {

        }
    }

    private void sendRegistrationToServer(Context context,String token)
    {
        CustomerData dataHelper = new CustomerData(context);
        dataHelper.updateGCMId(token);
        Log.i(TAG, "GCM Registration Token: " + token);
    }
}
