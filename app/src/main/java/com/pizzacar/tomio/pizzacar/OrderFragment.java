package com.pizzacar.tomio.pizzacar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.pizzacar.tomio.pizzacar.Data.CustomerData;
import com.pizzacar.tomio.pizzacar.Data.ItemData;
import com.pizzacar.tomio.pizzacar.Data.UserSharedPreferences;
import com.stripe.model.Order;

import java.util.HashMap;
import java.util.Set;

public class OrderFragment extends Fragment {
    private boolean canMakeOrder = true;

    public static final String ORDER_INFO = "order_info";
    public static final String ORDER_STRING = "order_string";
    public static final String ORDER_ID = "order_id";
    private final String TAG_NOT_YET = "TAG_NOT_YETS";

    private FloatingActionButton fab;
    private NumberPicker quantityPicker;
    private HashMap<Long,Long> shoppingCart;
    private String orderString; // /once the order is sumbited, response from the request will be stored here
    private ItemData dataHelper;
    private boolean orderExist;



    private final String TAG = OrderFragment.class.getSimpleName();


    private long[] orders ;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        shoppingCart = new HashMap<Long,Long>();

        GetOrderablesTask task = new GetOrderablesTask();
        task.execute();

    }


        public OrderFragment()
    {
        // Required empty public constructor
    }


    @Override
    public void onResume()
    {
        super.onResume();

        if(!canMakeOrder )
            refreshFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(com.pizzacar.tomio.pizzacar.R.layout.fragment_order, container, false);



        fab = (FloatingActionButton) view.findViewById(com.pizzacar.tomio.pizzacar.R.id.fab);

        fab.setImageResource(com.pizzacar.tomio.pizzacar.R.drawable.shopping_cart);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CheckOrderExistanceTask checkOrderExistanceTask = new CheckOrderExistanceTask();
                checkOrderExistanceTask.execute();


            }
        });

        return view;
    }

    public class GetOrderablesTask extends AsyncTask<Void,Void,String[][]>
    {
        private final String TAG = GetOrderablesTask.class.getSimpleName();
        private ProgressDialog dialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute()
        {
            //this.dialog.setMessage("Please wait");
            this.dialog.show();
        }
        @Override
        protected String[][] doInBackground(Void... params)
        {
            dataHelper = new ItemData(getActivity());
            return dataHelper.getOrderables();

        }


        @Override
        protected void onPostExecute(String[][] orderables)
        {

            FragmentManager fragmentManager = getChildFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            NotYetFragment notYetFragment = (NotYetFragment) fragmentManager.findFragmentByTag(TAG_NOT_YET);



            if(UserSharedPreferences.checkIsTester(getActivity()))
                canMakeOrder = true;
            else
                canMakeOrder = dataHelper.canMakeOrder();


            if(canMakeOrder)
                {
                    for (int i = 0; i < orderables.length; i++)
                    {


                        String[] itemInfo = orderables[i];

                        FoodItemFragment fragment = new FoodItemFragment();

                        Bundle bundle = new Bundle();
                        bundle.putString(ItemData.ITEM_IMAGE, itemInfo[ItemData.POSITION_ITEM_IMAGE]);
                        bundle.putString(ItemData.ITEM_NAME, itemInfo[ItemData.POSITION_ITEM_NAME]);
                        bundle.putDouble(ItemData.ITEM_PRICE, Double.parseDouble(itemInfo[ItemData.POSITION_ITEM_PRICE]));
                        bundle.putLong(ItemData.ITEM_ID, Long.parseLong(itemInfo[ItemData.POSITION_ITEM_ID]));
                        bundle.putInt(ItemData.ITEM_CART_STATUS, ItemData.ITEM_NOT_IN_CART);
                        bundle.putInt(FoodItemFragment.CURRENT_ACTIVITY, FoodItemFragment.ACTIVITY_MAIN);
                        bundle.putLong(ItemData.ITEM_QUANTITY, 1);

                        fragment.setArguments(bundle);

                        fragmentTransaction.add(com.pizzacar.tomio.pizzacar.R.id.food_item_container, fragment);
                }

                    if(notYetFragment != null)
                    {
                        Log.v(TAG, "IT IS NOT NULL");
                        fragmentTransaction.remove(notYetFragment);
                        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);

                    }
                    else
                        Log.v(TAG, "IT IS STILL NULL");
            }
            else
            {
                if(notYetFragment == null)
                {
                    Log.v(TAG, "IT IS NULL");
                    NotYetFragment fragment = new NotYetFragment();
                    fragmentTransaction.add(R.id.not_yet_container, fragment, TAG_NOT_YET);
                    fab.setVisibility(View.GONE);


                }
                else
                {
                    notYetFragment.finishRefreshing();
                    Log.v(TAG, "IT IS NOT NULL");

                }


            }
            fragmentTransaction.commitAllowingStateLoss();
            dialog.dismiss();
        }

    }

    public void refreshFragment()
    {
        GetOrderablesTask task = new GetOrderablesTask();
        task.execute();
    }


    public void addItemToShoppingCart(long itemId,long quanitity)
    {
        shoppingCart.put(itemId, quanitity);


    }

    public void removeItemFromShoppingCart(long itemId)
    {
        shoppingCart.remove(itemId);

    }

    public void updateShoppingCart(long itemId,long quantity)
    {
        shoppingCart.put(itemId, quantity);

    }


    public class CheckOrderExistanceTask extends AsyncTask<Void,Void,Boolean>
    {


        @Override
        protected Boolean doInBackground(Void... params)
        {

            CustomerData dataHelper = new CustomerData(getActivity());
            return dataHelper.orderExists();
        }

        @Override
        protected void onPostExecute(Boolean existance)
        {


            if (!existance)
            {
                if (shoppingCart.size() > 0) {
                    Set<Long> keySet = shoppingCart.keySet();
                    Long[] itemIds = keySet.toArray(new Long[keySet.size()]);

                    orders = new long[shoppingCart.size() * 2];
                    String orderInfo = "";

                    for (int i = 0; i < shoppingCart.size() * 2; i += 2) {


                        long itemId = itemIds[i / 2]; // the id of the item ordered
                        long quantity = (Long) shoppingCart.get(itemId); // the quantity of the item ordered
                        orderInfo += itemId + "," + quantity + ",";


                        orders[i] = itemId;
                        orders[i + 1] = quantity;

                    }


                    SubmitOrderTask submitTask = new SubmitOrderTask();
                    submitTask.execute(orderInfo);
                    Log.v(TAG, "The was called ");
                } else {
                    Toast.makeText(getActivity(), "There are no items in your order ", Toast.LENGTH_LONG).show();
                }

            }
            else
            {
                Toast.makeText(getActivity(),"Please wait until your existing order is delivered before making another one ",Toast.LENGTH_LONG).show();


            }




        }
    }
    public class SubmitOrderTask extends AsyncTask<String,Void,String>
    {

        @Override
        protected String doInBackground(String... params)
        {
            String orderInfo = params[0];

            dataHelper = new ItemData(getActivity());
            return dataHelper.submitInitialOrder(orderInfo);

        }

        @Override
        protected void onPostExecute(String orderJson)
        {

            if(orderJson != null)
            {
                Bundle bundle = new Bundle();
                bundle.putLongArray(ORDER_INFO, orders);
                bundle.putString(ORDER_STRING, orderJson);
                Log.v(TAG, "the newly made order" + orderJson);
                Intent intent = new Intent(getActivity(), ReviewActivity.class);
                intent.putExtras(bundle);


                startActivity(intent);
            }

            else
            {
                Toast.makeText(getActivity(), dataHelper.getErrorMessage(), Toast.LENGTH_LONG).show();
            }

        }
    }


    public void makeText(String message)
    {

        Toast.makeText(getActivity(),message,Toast.LENGTH_LONG).show();
    }

}
