package com.pizzacar.tomio.pizzacar.Driver;

import com.pizzacar.tomio.pizzacar.Data.DriverData;
import com.pizzacar.tomio.pizzacar.Data.UserSharedPreferences;
import com.pizzacar.tomio.pizzacar.Home;
import com.pizzacar.tomio.pizzacar.R;
import com.pizzacar.tomio.pizzacar.RegistrationIntentService;
import com.pizzacar.tomio.pizzacar.SignInActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import android.Manifest;
import android.annotation.TargetApi;
import com.google.android.gms.location.LocationServices;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.Toast;



public class DriverHome extends AppCompatActivity  implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient googleApiClient;
    private final String TAG_NEXT_ORDER_FRAG = "NEXT_ORDERS";
    private final String TAG_NO_ORDERS = "NORDSFS";
    private Location location;
    private static final int INITIAL_REQUEST=1337;


    private static final String[] LOCATION_PERMS={
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                      Manifest.permission.ACCESS_FINE_LOCATION
    };

    private final String TAG = DriverHome.class.getSimpleName();

     @TargetApi(Build.VERSION_CODES.M)
     @Override

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_home);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.container_next_order,new NewOrderFragment());
        transaction.commit();

        if (!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                  requestPermissions(LOCATION_PERMS, INITIAL_REQUEST);
        }
        else
            buildGoogleApiClient();

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            boolean sentToken = sharedPreferences.getBoolean(Home.SENT_TOKEN,false);
            if(!sentToken)
                startService(new Intent(this,RegistrationIntentService.class));



    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(DriverHome.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
               .addApi(LocationServices.API)
                .build();

        googleApiClient.connect();
    }

       @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

                        switch(requestCode){

                                case INITIAL_REQUEST:
                                if (!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION))
                                    {
                                                buildGoogleApiClient();
                                }
                                else
                                    Toast.makeText(DriverHome.this,"FAILURE", Toast.LENGTH_SHORT).show();
                                break;
                    }
            }


    @TargetApi (Build.VERSION_CODES.M)
       private boolean hasPermission(String perm)
         {
             return(PackageManager.PERMISSION_GRANTED==checkSelfPermission(perm));
         }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.driver_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        int id = item.getItemId();

        if (id == R.id.action_sign_out) {
            UserSharedPreferences.deletePreferences(DriverHome.this);
            Intent intent = new Intent(DriverHome.this,SignInActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            return true;
        }

        else if (id == R.id.action_update_inventory)
        {
            startActivity(new Intent(this,UpdateInventory.class));

        }

        return super.onOptionsItemSelected(item);
    }





    @Override
    public void onConnected(Bundle bundle) {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        try {
            manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,
                    new LocationListener() {
                        @Override
                        public void onStatusChanged(String provider, int status,
                                                    Bundle extras) {
                        }

                        @Override
                        public void onProviderEnabled(String provider) {
                        }

                        @Override
                        public void onProviderDisabled(String provider) {
                        }

                        @Override
                        public void onLocationChanged( Location location)
                        {
                            Log.v(TAG,"THE LOCATION IS "+location.getLatitude()+","+location.getLongitude());

                            UpdateLocationTask task = new UpdateLocationTask();
                            task.execute(location.getLatitude(),location.getLongitude());
                        }
                    });
             location = LocationServices.FusedLocationApi.getLastLocation(
                    googleApiClient);
            if (location != null)
            {
                Log.v(TAG,"THE LOCATION IS "+location.getLatitude()+","+location.getLongitude());
                UpdateLocationTask task = new UpdateLocationTask();
                task.execute(location.getLatitude(),location.getLongitude());
            }

            else
                Log.v(TAG,"asdfasdfaafas");


        }

        catch (SecurityException e)
        {
            Log.e(TAG,"There was a problem getting location: "+e.getMessage());
        }
    }

    @Override
    public void onConnectionSuspended(int i)
    {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
    public class UpdateLocationTask extends AsyncTask<Double,Void,Void>
    {
        private final String TAG = UpdateLocationTask.class.getSimpleName();
        @Override
        protected Void doInBackground(Double... params)
        {

            DriverData dataHelper = new DriverData(DriverHome.this);
            dataHelper.updateLocation(params[0],params[1]);
            return null;

        }

    }

}
