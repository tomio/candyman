package com.pizzacar.tomio.pizzacar;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class OrderInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(com.pizzacar.tomio.pizzacar.R.layout.activity_order_info);

        FragmentManager manager = getFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        OrderInfoFragment fragment = new OrderInfoFragment();
        fragmentTransaction.add(com.pizzacar.tomio.pizzacar.R.id.order_info_fragment_container,fragment);
        fragmentTransaction.commit();

    }


}
