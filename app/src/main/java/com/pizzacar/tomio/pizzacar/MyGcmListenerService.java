package com.pizzacar.tomio.pizzacar;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmListenerService;
import com.pizzacar.tomio.pizzacar.Data.UserSharedPreferences;
import com.pizzacar.tomio.pizzacar.Driver.DriverHome;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by tomio on 9/2/2016.
 */
public class MyGcmListenerService extends GcmListenerService
{

    private static final String TAG = "MyGcmListenerService";


    public static final int NOTIFICATION_ID = 1;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        // Time to unparcel the bundle!
        if (!data.isEmpty()) {



            String senderId = getString(R.string.gcm_default_sender);
            if (senderId.length() == 0) {
                Toast.makeText(this, "SenderID string needs to be set", Toast.LENGTH_LONG).show();
            }
            // Not a bad idea to check that the message is coming from your server.
            if ((senderId).equals(from)) {
                // Process message and then post a notification of the received message.

                    sendNotification(data.getString("message"));

            }
        }
    }



        private void sendNotification(String message) {
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);   
        PendingIntent contentIntent;

        if(UserSharedPreferences.getUserId(this) != 3 )
            contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, Home.class), 0);
        else
            contentIntent = PendingIntent.getActivity(this, 0, new Intent(this, DriverHome.class), 0);
        Bitmap largeIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.pizza);


            NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.pizza  )
                        .setDefaults(Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND | Notification.FLAG_SHOW_LIGHTS)
                        .setLargeIcon(largeIcon)
                        .setContentTitle(message)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        //.setContentText(message)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }



}
