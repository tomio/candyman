package com.pizzacar.tomio.pizzacar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.pizzacar.tomio.pizzacar.Data.CustomerData;


public class AlarmReceiver extends BroadcastReceiver {
    private static String TAG = AlarmReceiver.class.getSimpleName();
    private AlarmManager alarmMgr;
    @Override
    public void onReceive(Context context, Intent intent) {
        /*String message = "Hellooo, alrm worked ----";
        Log.v(TAG, "Alrmaaaaa SET !!");
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();*/
        Intent i = new Intent("GET_ME");
        i.putExtra(CustomerData.CUSTOMER_TIME_REMAINING,"17");

        context.sendBroadcast(i);


    }

    public  void setAlarm(Context context)
    {
        Log.v(TAG, "Alrmaaaaa SET !!");

         Intent intent = new Intent(context,AlarmReceiver.class);
         PendingIntent pendingIntent = PendingIntent.getBroadcast(context,0,intent,0);
         alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
         alarmMgr.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), 1000, pendingIntent);

    }
}