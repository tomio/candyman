package com.pizzacar.tomio.pizzacar;

/**
 * Created by tomio on 5/31/2016.
 */

        import android.annotation.TargetApi;
        import android.app.Activity;
        import android.view.Window;
        import android.view.WindowManager;

/**
 * Created by tomio on 3/3/2016.
 */
public class Aesthetic
{

    public String getFoodeyTitleText()
    {

        String text =
                        "<font color=#33cc33>P</font>" +
                        "<font color=#FF6347>i</font>" +
                        "<font color=#FF6347>z</font>" +
                        "<font color=#9999ff>a</font>" +
                        "<font color=#33cc33>z</font>" +
                        "<font color=#ffcc00>z</font>" +
                        "<font color=#33cc33>a</font>" +
                        "<font color=#FF6347>n</font>"
                ;

        return text ;

    }

    @TargetApi(21)
    @SuppressWarnings("deprecation")
    public static void setBarColor(Activity activity)
    {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if(currentapiVersion >= 21)
        {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(activity.getResources().getColor(com.pizzacar.tomio.pizzacar.R.color.orange));

        }


    }

}