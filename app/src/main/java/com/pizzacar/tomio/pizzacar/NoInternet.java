package com.pizzacar.tomio.pizzacar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.pizzacar.tomio.pizzacar.Data.Request;

public class NoInternet extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internet);

        findViewById(R.id.retry_internet_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                new AsyncTask<Void, Void, Boolean>() {
                    private ProgressDialog dialog = new ProgressDialog(NoInternet.this);

                    @Override
                    protected void onPreExecute()
                    {
                        //this.dialog.setMessage("Please wait");
                        this.dialog.show();
                    }

                    @Override
                    protected Boolean doInBackground(Void... params) {

                        Request request = new Request(NoInternet.this);
                        return request.isInternetAvailable();


                    }

                    protected void onPostExecute(Boolean isConnected)
                    {
                        dialog.dismiss();
                        if(isConnected)
                            startActivity(new Intent(NoInternet.this,ActivitySignUp.class));
                    }
                }.execute();
            }
        });
    }



}
