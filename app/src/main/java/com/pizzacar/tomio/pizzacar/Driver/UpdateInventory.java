package com.pizzacar.tomio.pizzacar.Driver;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.pizzacar.tomio.pizzacar.Data.DriverData;
import com.pizzacar.tomio.pizzacar.Data.ItemData;
import com.pizzacar.tomio.pizzacar.R;

public class UpdateInventory extends AppCompatActivity {

    public String[] fragmentIds;
    private final String TAG = UpdateInventory.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_inventory);
        GetOrderablesTask task = new GetOrderablesTask();
        task.execute();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        Button updateButton =(Button) findViewById(R.id.button_update_inventory);

        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                FragmentManager manager = getFragmentManager();
                String updateString = "";
                for(int i = 0; i < fragmentIds.length; i++)
                {
                    OrderableFieldFragment frag = (OrderableFieldFragment) manager.findFragmentByTag(fragmentIds[i]);
                    long id = frag.getOrderableId();
                    int quantity = frag.getQuantity();

                    if(i == 0)
                        updateString+=id+","+quantity;

                    else
                        updateString+=","+id+","+quantity;



                }

                UpdateInvetoryTask task = new UpdateInvetoryTask();
                Log.v(TAG,"the update String is "+updateString);
                task.execute(updateString);
            }
        });
    }




    public class UpdateInvetoryTask extends AsyncTask<String,Void,Void>
    {


        private final String TAG = GetOrderablesTask.class.getSimpleName();

        @Override
        protected Void doInBackground(String... params)
        {


            DriverData dataHelper = new DriverData(UpdateInventory.this);


             dataHelper.updateInventory(params[0]);

             return null;

        }

        @Override
        protected void onPostExecute(Void p)
        {
            Intent intent = new Intent(UpdateInventory.this,DriverHome.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        }


    }






    public class GetOrderablesTask extends AsyncTask<Void,Void,String[][]>
    {


        private final String TAG = GetOrderablesTask.class.getSimpleName();

        @Override
        protected String[][] doInBackground(Void... params)
        {


            ItemData dataHelper = new ItemData(UpdateInventory.this);


            return dataHelper.getOrderables();


        }


        @Override
        protected void onPostExecute(String[][] orderables)
        {


            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            fragmentIds = new String[orderables.length];
            for(int i = 0; i < orderables.length; i++)
            {


                String[] itemInfo = orderables[i];

                OrderableFieldFragment fragment = new OrderableFieldFragment();


                long orderableId = Long.parseLong(itemInfo[ItemData.POSITION_ITEM_ID]);
                String name = itemInfo[ItemData.POSITION_ITEM_NAME];
                fragment.setOrderableName(name);
                fragment.setOrderableId(orderableId);
                String fragTag = "Frag"+i;
                fragmentIds[i] = fragTag;
                fragmentTransaction.add(R.id.orderable_field_container, fragment,fragTag);


            }
            fragmentTransaction.commitAllowingStateLoss();

        }

    }

}
