package com.pizzacar.tomio.pizzacar.Data;

import android.content.Context;

/**
 * Created by tomio on 8/8/2016.
 */
public class DriverData
{
    private Context context;
    public DriverData(Context context)
    {
        this.context = context;
    }

    public String getNextOrder()
    {
        Request request = new Request(context);

        return request.makeRequest("next_order","GET","");
    }

    public void setDelivered(long orderId)
    {

        Request request = new Request(context);

        request.makeRequest("set_delivered","POST","order_id="+orderId);

    }

    public void updateLocation(double lat,double lon)
    {
        String location = lat+","+lon;
        Request request = new Request(context);
        request.makeRequest("update_location","POST","location="+location);

    }
    public void updateGCMId()
    {

    }
    public void updateInventory(String inventory)
    {
        Request request = new Request(context);
        request.makeRequest("update_inventory","POST","inventory="+inventory);

    }

}
