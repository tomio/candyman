package com.pizzacar.tomio.pizzacar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pizzacar.tomio.pizzacar.Driver.DriverHome;
import com.pizzacar.tomio.pizzacar.Data.CustomerData;
import com.pizzacar.tomio.pizzacar.Data.UserSharedPreferences;

public class SignInActivity extends Activity {

    private EditText emailField;
    private EditText passwordField;
    private String email;
    private String password;
    private TextView signUpText;
    Button signInButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.pizzacar.tomio.pizzacar.R.layout.activity_sign_in);

        emailField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.field_email);
        passwordField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.field_password);
        signInButton = (Button) findViewById(com.pizzacar.tomio.pizzacar.R.id.button_sign_in);
        signUpText = (TextView) findViewById(com.pizzacar.tomio.pizzacar.R.id.text_sign_up);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                SignInTask task = new SignInTask();
                task.execute(emailField.getText().toString().trim(),passwordField.getText().toString());

            }
        });

        signUpText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(SignInActivity.this,ActivitySignUp.class));
            }
        });
        Aesthetic aesthetic = new Aesthetic();
        aesthetic.setBarColor(this);

        TextView applicationTitle = (TextView) findViewById(com.pizzacar.tomio.pizzacar.R.id.application_title);

       // applicationTitle.setText(Html.fromHtml(aesthetic.getFoodeyTitleText()));


    }

   public class SignInTask extends AsyncTask<String,Void,Long>
    {

        private ProgressDialog dialog = new ProgressDialog(SignInActivity.this);

        @Override
        protected void onPreExecute()
        {
            //this.dialog.setMessage("Please wait");
            this.dialog.show();
        }
        @Override
        protected Long doInBackground(String... params)
        {

           CustomerData dataHelper = new CustomerData(SignInActivity.this);

             email = params[0];
             password = params[1];

            return dataHelper.login(email, password);
        }

        @Override
        public void onPostExecute(Long userId)
        {
            dialog.dismiss();
            if(userId > 0)
            {

                UserSharedPreferences.setUserEmail(SignInActivity.this, email);
                UserSharedPreferences.setUserId(SignInActivity.this, userId);
                UserSharedPreferences.setUserPassword(SignInActivity.this, password);

                if(userId == 3)
                {
                    Intent intent = new Intent(SignInActivity.this, DriverHome.class);

                    startActivity(intent);

                }

                else
                {
                    Intent intent = new Intent(SignInActivity.this, Home.class);

                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                }

            }
            else
                Toast.makeText(SignInActivity.this,"Incorrect username or password",Toast.LENGTH_LONG).show();

            }
        }
}
