package com.pizzacar.tomio.pizzacar.Data;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Base64;
import android.util.Log;

import com.pizzacar.tomio.pizzacar.NoInternet;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * Created by tomio on 5/19/2016.
 */
public class Request
{
    private final String TAG = Request.class.getSimpleName();
    public static final String BASE_URL = "http://45.55.140.148/api/";
    private int statusCode;

    private Context context ;

    public Request(Context c)
    {
        context = c;

    }

    public boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }

    }




    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String makeRequest(String dataUrl,String requestMethod,String params)
    {


        if(!isInternetAvailable())
        {
            context.startActivity(new Intent(context, NoInternet.class));
            return "";
        }
        HttpURLConnection con = null;
        BufferedReader reader = null;
        String jsonString = "";
        String email = UserSharedPreferences.getUserEmail(context).trim();
        String password = UserSharedPreferences.getUserPassword(context).trim();


        String userCredentials = email+":"+password;



        try
        {

            Uri uri = Uri.parse(BASE_URL+dataUrl);

            String urlString = uri.toString();

            if(requestMethod.equals("GET"))
                urlString+="?"+params;
            URL url = new URL(urlString);
            Log.v(TAG, "Url String: " + urlString);

            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod( requestMethod);
            String basicAuth = "Basic " + new String(Base64.encode(userCredentials.getBytes("UTF-8"),Base64.DEFAULT));
            con.setRequestProperty("Authorization", basicAuth);


            if(requestMethod.equals("POST") )
            {

                byte[] postData = params.getBytes( StandardCharsets.UTF_8);
                int dataLength = postData.length;
                con.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
                con.setRequestProperty( "charset", "utf-8");
                con.setRequestProperty( "Content-Length", Integer.toString( dataLength ));

                try( DataOutputStream wr = new DataOutputStream( con.getOutputStream())) {
                    wr.write( postData );

                }
            }


            this.statusCode= con.getResponseCode();
            Log.v(TAG,"Response code"+this.statusCode);

            InputStream in;

            if(this.statusCode == 200)
                in= con.getInputStream();
            else
                in = con.getErrorStream();
            StringBuffer buffer = new StringBuffer();

            if (in == null)
            {
                Log.e(TAG, "There was a problem getting the json String");
            }

            reader = new BufferedReader(new InputStreamReader(in));
            String line;

            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");

            }

            if (buffer.length() == 0) {
                return null;

            }
            jsonString = buffer.toString();
            Log.v(TAG, "Json String : " + jsonString);



        }

        catch (IOException e) {
            Log.e(TAG, "Error ", e);
        } finally {
            if (con != null)
                con.disconnect();
        }

        if (reader != null) {
            try {
                reader.close();

            } catch (final IOException e) {
                Log.e(TAG, "There was a problem closing the reader ", e);

            }
        }

        return jsonString;
    }


    public  long parseUserId(String jsonString)
            throws JSONException
    {
        JSONArray userIdArray = new JSONArray(jsonString);
        long userId = userIdArray.getJSONObject(0).getLong("id");
        return userId;

    }


    public String update(String urlString,String params)
    {
        URL url = null;
        String email = UserSharedPreferences.getUserEmail(context);
        String password = UserSharedPreferences.getUserPassword(context);

        String userCredentials = email+":"+password;
        BufferedReader reader = null;
        String jsonString = null;

        Log.v(TAG,"THE PARAMS FOR THE PUT "+params);
        try
        {
            url = new URL(BASE_URL+urlString);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            byte[] putData = params.getBytes("UTF-8");

            String basicAuth = "Basic " + new String(Base64.encode(userCredentials.getBytes("UTF-8"),Base64.DEFAULT));
            con.setRequestProperty("Authorization", basicAuth);
            con.setRequestProperty("Content-length", putData.length + "");

            con.setDoOutput(true);
            con.setRequestMethod("PUT");
            DataOutputStream wr = new DataOutputStream( con.getOutputStream());
            wr.write( putData );

             this.statusCode = con.getResponseCode();

            InputStream in;
            if(this.statusCode == 200)
                in= con.getInputStream();
            else
                in = con.getErrorStream();

            StringBuffer buffer = new StringBuffer();

            if (in == null)
            {
                // return null;
                Log.e(TAG, "There was a problem getting the json String");
            }

            reader = new BufferedReader(new InputStreamReader(in));
            String line;

            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");

            }

            if (buffer.length() == 0) {
              //  return null;

            }
            jsonString = buffer.toString();
            Log.v(TAG, "Json String : " + jsonString);





        } catch (IOException e) {
            e.printStackTrace();
        }


        return jsonString;


    }


    public int getStatusCode()
    {
        return this.statusCode;
    }



}