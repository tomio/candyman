package com.pizzacar.tomio.pizzacar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pizzacar.tomio.pizzacar.Data.CustomerData;

import org.json.JSONException;
import org.json.JSONObject;

public class PromotionsActivity extends AppCompatActivity {
    public static final String PROMOTION_DESC = "desc";
    public static final String NEW_ORDER_TOTAL = "new_total";
    private String orderString;
    private long[] orders;
    private long orderId;
    private final String TAG = PromotionsActivity.class.getSimpleName();


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(com.pizzacar.tomio.pizzacar.R.layout.activity_promotions);

        Intent intent = getIntent();
        orderId = intent.getLongExtra(ReviewActivity.ORDER_ID,-1);
        final EditText fieldPromo = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.field_promo);
        Button addPromo = (Button) findViewById(com.pizzacar.tomio.pizzacar.R.id.button_add_promo);
        orderString = intent.getStringExtra(MainActivity.ORDER_STRING);
        orders = intent.getLongArrayExtra(MainActivity.ORDER_INFO);

        addPromo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                AddPromoTask task = new AddPromoTask();
                task.execute(fieldPromo.getText().toString().trim());
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public class AddPromoTask extends AsyncTask<String,Void,String>
    {
        @Override
        protected String doInBackground(String... params)
        {
            CustomerData dataHelper = new CustomerData(PromotionsActivity.this);
            return dataHelper.addPromo(params[0],orderId );

        }

        @Override
        protected void onPostExecute(String promoString)
        {
            try
            {
                JSONObject json = new JSONObject(promoString);
                String acceptace = json.getString("accepted");
                switch(acceptace)
                {
                    case "true":
                        double total = json.getDouble("total");
                        String promoDesc = json.getString("message");
                        Log.v(TAG,"The order string here is "+orderString);
                        Toast.makeText(PromotionsActivity.this,"Promo Added",Toast.LENGTH_LONG).show();
                        Bundle bundle = new Bundle();
                        bundle.putString(PROMOTION_DESC, promoDesc);
                        bundle.putDouble(NEW_ORDER_TOTAL, total);
                        bundle.putLongArray(MainActivity.ORDER_INFO, orders);
                        bundle.putString(MainActivity.ORDER_STRING,orderString);
                        Intent intent = new Intent(PromotionsActivity.this,ReviewActivity.class);
                        intent.putExtras(bundle);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        startActivity(intent);

                        break;
                    case "false":
                        Toast.makeText(PromotionsActivity.this,"Promo NOT found",Toast.LENGTH_LONG).show();
                        break;
                    case "used":
                        String message = json.getString("message");
                        Toast.makeText(PromotionsActivity.this,message,Toast.LENGTH_LONG).show();
                        break;
                    case "expired":
                        final Intent expiredIntent = new Intent(PromotionsActivity.this, Home.class);
                        expiredIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        new AlertDialog.Builder(PromotionsActivity.this)
                                .setMessage("Promotion added, but your order has expired. Please make another one.")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which)
                                    {

                                        startActivity(expiredIntent);

                                    }
                                }).show();

                        break;
                    default:
                        break;


                }




            }
            catch(JSONException e)
            {

            }
        }
    }
}
