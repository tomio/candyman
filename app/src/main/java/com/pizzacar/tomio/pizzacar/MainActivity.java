package com.pizzacar.tomio.pizzacar;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.pizzacar.tomio.pizzacar.Data.ItemData;
import com.pizzacar.tomio.pizzacar.Data.UserSharedPreferences;

import java.util.HashMap;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    public static final String ORDER_INFO = "order_info";
    public static final String ORDER_STRING = "order_string";
    public static final String ORDER_ID = "order_id";
    private NumberPicker quantityPicker;
    private HashMap<Long,Long> shoppingCart;
    private String orderString; // /once the order is sumbited, response from the request will be stored here
    private final String TAG = MainActivity.class.getSimpleName();
    private ActionBarDrawerToggle toggleListener;

    private DrawerLayout drawer;
    private ListView navigationList;
    private NavigationAdapter navigationAdapter;

    private long[] orders ;

    private final int POSITION_ORDERS = 1;
    private final int POSITION_ACCOUNT = 2;
    private final int POSITION_SIGN_OUT = 3;
    private ItemData dataHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(com.pizzacar.tomio.pizzacar.R.layout.activity_main);


        GetOrderablesTask task = new GetOrderablesTask();
        task.execute();

        Toolbar toolbar = (Toolbar) findViewById(com.pizzacar.tomio.pizzacar.R.id.toolbar);
        drawer = (DrawerLayout) findViewById(com.pizzacar.tomio.pizzacar.R.id.drawer_layout);
        navigationList = (ListView) findViewById(com.pizzacar.tomio.pizzacar.R.id.navigation_list_view);
        navigationAdapter = new NavigationAdapter(this);
        navigationList.setAdapter(navigationAdapter);
        navigationList.setOnItemClickListener(this);

        toggleListener = new ActionBarDrawerToggle(this,drawer,toolbar, com.pizzacar.tomio.pizzacar.R.string.open_drawer, com.pizzacar.tomio.pizzacar.R.string.close_drawer)
        {
            @Override
            public void onDrawerClosed(View drawerView)
            {

                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView)
            {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void setDrawerIndicatorEnabled(boolean enable) {
                super.setDrawerIndicatorEnabled(enable);
            }
        };
        setSupportActionBar(toolbar);
        drawer.setDrawerListener(toggleListener);




        FloatingActionButton fab = (FloatingActionButton) findViewById(com.pizzacar.tomio.pizzacar.R.id.fab);
        fab.setImageResource(com.pizzacar.tomio.pizzacar.R.drawable.shopping_cart);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (shoppingCart.size() > 0)
                {
                    Set<Long> keySet = shoppingCart.keySet();
                    Long[] itemIds = keySet.toArray(new Long[keySet.size()]);

                    orders = new long[shoppingCart.size() * 2];
                    String orderInfo = "";

                    for (int i = 0; i < shoppingCart.size() * 2; i += 2) {


                        long itemId = itemIds[i / 2]; // the id of the item ordered
                        long quantity = (Long) shoppingCart.get(itemId); // the quantity of the item ordered
                        orderInfo += itemId + "," + quantity + ",";


                        orders[i] = itemId;
                        orders[i + 1] = quantity;

                    }


                    SubmitOrderTask submitTask = new SubmitOrderTask();
                    submitTask.execute(orderInfo);
                    Log.v(TAG, "The was called ");
                }

                else
                {
                    Toast.makeText(MainActivity.this,"There are no items in your order ",Toast.LENGTH_LONG).show();
                }

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(com.pizzacar.tomio.pizzacar.R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        int id = item.getItemId();

        if (toggleListener  .onOptionsItemSelected(item)) {
            return true;
        }

        if (id == com.pizzacar.tomio.pizzacar.R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addItemToShoppingCart(long itemId,long quanitity)
    {
        shoppingCart.put(itemId, quanitity);


    }

    public void removeItemFromShoppingCart(long itemId)
    {
        shoppingCart.remove(itemId);

    }

    public void updateShoppingCart(long itemId,long quantity)
    {
        shoppingCart.put(itemId,quantity);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        switch (position)
        {
            case POSITION_ACCOUNT:

                startActivity( new Intent(MainActivity.this,AccountInfoActivity.class));
                break;
            case POSITION_ORDERS:
                break;

            case POSITION_SIGN_OUT:
                UserSharedPreferences.deletePreferences(MainActivity.this);
                Intent intent = new Intent(MainActivity.this,SignInActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                startActivity(intent);

                break ;
        }

        drawer.closeDrawers();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        toggleListener.syncState();
    }




    public class GetOrderablesTask extends AsyncTask<Void,Void,String[][]>
    {





        private final String TAG = GetOrderablesTask.class.getSimpleName();

        @Override
        protected String[][] doInBackground(Void... params)
        {


            ItemData dataHelper = new ItemData(MainActivity.this);


            return dataHelper.getOrderables();


        }


        @Override
        protected void onPostExecute(String[][] orderables)
        {

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();


            for(int i = 0; i < orderables.length; i++)
            {


                String[] itemInfo = orderables[i];

                FoodItemFragment fragment = new FoodItemFragment();

                Bundle bundle = new Bundle();
                bundle.putInt(ItemData.ITEM_IMAGE, com.pizzacar.tomio.pizzacar.R.drawable.honeybun);
                bundle.putString(ItemData.ITEM_NAME, itemInfo[ItemData.POSITION_ITEM_NAME]);
                bundle.putDouble(ItemData.ITEM_PRICE, Double.parseDouble(itemInfo[ItemData.POSITION_ITEM_PRICE]));
                bundle.putLong(ItemData.ITEM_ID, Long.parseLong(itemInfo[ItemData.POSITION_ITEM_ID]));
                bundle.putInt(ItemData.ITEM_CART_STATUS, ItemData.ITEM_IN_CART);
                bundle.putInt(FoodItemFragment.CURRENT_ACTIVITY,FoodItemFragment.ACTIVITY_MAIN);
                bundle.putLong(ItemData.ITEM_QUANTITY,1);

                fragment.setArguments(bundle);

               // fragmentTransaction.add(R.id.food_item_container, fragment);



            }
            fragmentTransaction.commit();

        }

    }


    public class SubmitOrderTask extends AsyncTask<String,Void,String>
    {


        @Override
        protected String doInBackground(String... params)
        {
            String orderInfo = params[0];

             dataHelper = new ItemData(MainActivity.this);
            return dataHelper.submitInitialOrder(orderInfo);


        }

        @Override
        protected void onPostExecute(String orderJson)
        {

            if(orderJson != null)
            {
                Bundle bundle = new Bundle();
                bundle.putLongArray(ORDER_INFO, orders);
                bundle.putString(ORDER_STRING, orderJson);
                Log.v(TAG, "the newly made order" + orderString);

                Intent intent = new Intent(MainActivity.this, ReviewActivity.class);
                intent.putExtras(bundle);


                startActivity(intent);
            }

            else
            {
                Toast.makeText(MainActivity.this,dataHelper.getErrorMessage(),Toast.LENGTH_LONG).show();
            }

        }
    }

    class NavigationAdapter extends BaseAdapter
    {
        String[] navigationArray;
        private Context context;
        public NavigationAdapter(Context context)
        {

            navigationArray = context.getResources().getStringArray(com.pizzacar.tomio.pizzacar.R.array.nagivation);
            this.context = context;
        }

        @Override
        public int getCount() {
            return navigationArray.length;
        }

        @Override
        public Object getItem(int position) {
            return navigationArray[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row;


            if(convertView == null)
            {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(com.pizzacar.tomio.pizzacar.R.layout.navigation_drawer_item_layout,parent,false);
                TextView rowText = (TextView) row.findViewById(com.pizzacar.tomio.pizzacar.R.id.row_text);



                rowText.setText(navigationArray[position]);
            }

            else
            {
                row = convertView;
            }

            return row;
        }
    }












}
