package com.pizzacar.tomio.pizzacar.Driver;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.pizzacar.tomio.pizzacar.R;


public class OrderableFieldFragment extends Fragment {

    private long orderableId;
    private EditText quantityField;
    private TextView textName;
    private String orderableName;
    public OrderableFieldFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_orderable_field, container, false);
        quantityField  = (EditText) view.findViewById(R.id.field_orderable_quantity);
        textName = (TextView) view.findViewById(R.id.text_orderable_name);
        textName.setText(orderableName);

        return view;
    }

    public void setOrderableId(long orderableId)
    {
        this.orderableId = orderableId;

    }

    public void setOrderableName(String orderableName)
    {
        this.orderableName = orderableName;

    }

    public int getQuantity()
    {
        return Integer.parseInt(quantityField.getText().toString());
    }
    public long getOrderableId() {
     return this.orderableId;

    }

}
