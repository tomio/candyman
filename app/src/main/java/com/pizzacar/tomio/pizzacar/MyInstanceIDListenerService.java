package com.pizzacar.tomio.pizzacar;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by tomio on 9/2/2016.
 */
public class MyInstanceIDListenerService extends InstanceIDListenerService
{
    private final String TAG = MyInstanceIDListenerService.class.getSimpleName();

    @Override
    public void onTokenRefresh()
    {
        Intent intent = new Intent(this,RegistrationIntentService.class);
        startService(intent);


    }



}
