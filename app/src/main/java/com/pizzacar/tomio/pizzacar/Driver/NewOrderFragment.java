package com.pizzacar.tomio.pizzacar.Driver;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.pizzacar.tomio.pizzacar.Data.DriverData;
import com.pizzacar.tomio.pizzacar.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class NewOrderFragment extends Fragment
{
    private TextView textTotal;
    private TextView textAddress;
    private TextView textOrdersLeft;
    private Button nextOrder;
    private long orderId;
    private ArrayList<String> fragmentTags;
    public NewOrderFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view =  inflater.inflate(R.layout.fragment_new_order, container, false);


        textTotal = (TextView) view.findViewById(R.id.customer_order_total);
        textAddress = (TextView) view.findViewById(R.id.customer_address);
        nextOrder = (Button) view.findViewById(R.id.button_set_order_deliver);
        textOrdersLeft = (TextView) view.findViewById(R.id.text_orders_left);
        nextOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetOrderDelivered setTask = new SetOrderDelivered();
                setTask.execute();

            }
        });


        GetNextOrderTask task = new GetNextOrderTask();
        task.execute();
        fragmentTags = new ArrayList<String>();


        return view;
    }


    public void addItemFragments(JSONArray items)
            throws JSONException
    {
        FragmentManager manager = getChildFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        if(fragmentTags.size() != 0) {
            clearFragment(manager, transaction);
        }

        for(int i = 0; i < items.length(); i++)
        {

            JSONObject item = items.getJSONObject(i);
            String quantity = item.getString("quantity");
            String itemName = item.getString("item_name");

            ItemInfoFragment fragment = new ItemInfoFragment();
            fragment.setItemInfo(quantity, itemName);
            String fragTag = "TAGBOI"+i;
            transaction.add(R.id.linearLayout,fragment,fragTag);
            fragmentTags.add(fragTag);

        }

        transaction.commitAllowingStateLoss();

    }

    private void clearFragment(FragmentManager manager,FragmentTransaction transaction)
    {

        for(int i =0; i < fragmentTags.size(); i++)
        {
            ItemInfoFragment fragment = (ItemInfoFragment)manager.findFragmentByTag(fragmentTags.get(i));
            transaction.remove(fragment);
        }

        fragmentTags.clear();

    }

    public class GetNextOrderTask extends AsyncTask<Void,Void,String>
    {
        private final String TAG = GetNextOrderTask.class.getSimpleName();
        @Override
        protected String doInBackground(Void... params)
        {

            DriverData dataHelper = new DriverData(getActivity());
            return dataHelper.getNextOrder();

        }

        @Override
        protected void onPostExecute(String jsonString)
        {

            try
            {
                JSONObject json = new JSONObject(jsonString);

                JSONObject orderInfo = json.getJSONObject("order");
                if(orderInfo.length() > 0)
                {
                    String ordersLeft = json.getString("orders_left");
                    textOrdersLeft.setText("Orders Left: "+ordersLeft);
                    if(orderInfo.length() > 0)
                    {
                        final String address = orderInfo.getString("location");
                        textAddress.setText(address);
                        textAddress.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                                        Uri.parse("http://maps.google.com/maps?daddr=" + address));
                                startActivity(intent);
                            }
                        });

                        textTotal.setText("$"+orderInfo.getString("total"));
                        orderId = orderInfo.getLong("id");
                        JSONArray items = json.getJSONArray("items_info");

                        addItemFragments(items);


                    }

                }

                else
                {
                    textAddress.setText("No more orders ");

                }


            }
            catch(JSONException e)
            {
                Log.e(TAG, "There was a problem setting driver home", e);
            }
        }




    }


    public class SetOrderDelivered extends AsyncTask<Void,Void,Void>
    {
        private final String TAG = GetNextOrderTask.class.getSimpleName();
        @Override
        protected Void doInBackground(Void... params)
        {

            DriverData dataHelper = new DriverData(getActivity());
            dataHelper.setDelivered(orderId);
            return null;

        }

        @Override
        protected void onPostExecute(Void d)
        {

            GetNextOrderTask task = new GetNextOrderTask();
            task.execute();
        }

    }


}



