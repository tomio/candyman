package com.pizzacar.tomio.pizzacar;

import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class NotYetFragment extends Fragment {
    private SwipeRefreshLayout swipeContainer;

    public NotYetFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(com.pizzacar.tomio.pizzacar.R.layout.fragment_not_yet, container, false);

         swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh()
            {
                ((OrderFragment)getParentFragment()).refreshFragment();
            }
        });


        return view;
    }

    public void finishRefreshing()
    {
        swipeContainer.setRefreshing(false);
    }




}


