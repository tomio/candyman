package com.pizzacar.tomio.pizzacar;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.pizzacar.tomio.pizzacar.Data.CustomerData;
import com.pizzacar.tomio.pizzacar.Data.UserSharedPreferences;

public class EditAccountActivity extends AppCompatActivity
{
    private Menu menu;
    EditText addressField;
    private EditText updateCardField;
    private String email;
    private String address;
    private String payment;
    private CustomerData dataHelper;
    private EditText emailField;
    private View  checkActionView;

    private static final String TAG = EditAccountActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(com.pizzacar.tomio.pizzacar.R.layout.activity_account);

        Bundle args = getIntent().getExtras();

        email = args.getString(CustomerData.CUSTOMER_EMAIL);
        address = args.getString(CustomerData.CUSTOMER_ADDRESS);
        payment = args.getString(CustomerData.CUSTOMER_PAYMENT);





        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        addressField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.field_update_address);
        updateCardField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.field_update_payment);
        emailField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.field_update_email);
        addressField.setText(address);
        emailField.setText(email);

        if(!payment.equals("new"))
            updateCardField.setText(payment);
        else
            updateCardField.setHint(com.pizzacar.tomio.pizzacar.R.string.hint_no_payment);


        updateCardField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(EditAccountActivity.this,UpdateCardActivity.class);
                email = emailField.getText().toString();
                address = addressField.getText().toString();

                Bundle bundle = new Bundle();

                bundle.putString(CustomerData.CUSTOMER_EMAIL,email);
                bundle.putString(CustomerData.CUSTOMER_ADDRESS,address);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        this.menu = menu;
        getMenuInflater().inflate(com.pizzacar.tomio.pizzacar.R.menu.account_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case com.pizzacar.tomio.pizzacar.R.id.action_check:
                setRefreshActionButtonState(true);
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    public void setRefreshActionButtonState(boolean show)
    {
        if (menu != null)
        {

            UpdatedInfoTask task = new UpdatedInfoTask();

            final MenuItem refreshItem = menu.findItem(com.pizzacar.tomio.pizzacar.R.id.action_check);
            if (refreshItem != null)
            {
                if(show)
                {
                    checkActionView =  refreshItem.getActionView();

                    refreshItem.setActionView(com.pizzacar.tomio.pizzacar.R.layout.actionbar_progress);
                    task.execute(emailField.getText().toString().trim(), addressField.getText().toString());
                    Log.v(TAG, "Def Not Collapsed");

                }
                else
                {
                    Log.v(TAG, "I wasnt called");
                    refreshItem.setActionView(checkActionView);

                }

            }


        }

        else
            Log.v(TAG, "Not Collapsed");
    }

    public class UpdatedInfoTask extends AsyncTask<String,Void,Boolean>
    {


        @Override
        protected Boolean doInBackground(String... params)
        {

            email = params[0]; // the email that the user wants on their account
            address = params[1];

            dataHelper = new CustomerData(EditAccountActivity.this);
            return dataHelper.updateUserInfo(email,address);

        }

        @Override
        protected void onPostExecute(Boolean success)
        {
            if(success)
            {
                Intent intent = new Intent(EditAccountActivity.this, AccountInfoActivity.class);
                UserSharedPreferences.setUserEmail(EditAccountActivity.this,email);
               // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }

            else
            {
                Toast.makeText(EditAccountActivity.this,dataHelper.getSignUpErrorMessage(),Toast.LENGTH_LONG).show();
                setRefreshActionButtonState(false);

            }
        }


    }


}
