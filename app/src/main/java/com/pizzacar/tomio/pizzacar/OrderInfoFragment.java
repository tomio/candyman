package com.pizzacar.tomio.pizzacar;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
    import android.widget.TextView;

import com.pizzacar.tomio.pizzacar.Data.CustomerData;


public class OrderInfoFragment extends Fragment {
    private final String TAG = OrderInfoFragment.class.getSimpleName();
    private TextView timeText;
    private TextView textMins;
    private TextView textInfo;
    private AlarmManager alarmMgr;
    private int devider = 1;
    PendingIntent pendingIntent;
    private long orderId =-1;
    private boolean shouldSetAlarm = true;

    public OrderInfoFragment() {}

    public static Fragment h()
    {
        return new OrderInfoFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        GetTimeRemainingTask task = new GetTimeRemainingTask();
        task.execute();

    }
    @Override
    public void onResume()
    {
        super.onResume();


        if(shouldSetAlarm)
        {
            Intent intent = new Intent(getActivity(),AlarmReceiver.class);
            pendingIntent = PendingIntent.getBroadcast(getActivity(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmMgr = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
            alarmMgr.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), 1000, pendingIntent);
        }
        }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(com.pizzacar.tomio.pizzacar.R.layout.fragment_order_info, container, false);
        timeText = (TextView) view.findViewById(com.pizzacar.tomio.pizzacar.R.id.text_time_remaining);
        textInfo = (TextView) view.findViewById(com.pizzacar.tomio.pizzacar.R.id.text_time_info);
        textMins = (TextView) view.findViewById(com.pizzacar.tomio.pizzacar.R.id.text_mins);

        return view;
    }

    public class GetTimeRemainingTask extends AsyncTask<Void,Void,Long>
    {

        private String TAG = GetTimeRemainingTask.class.getSimpleName();

        private ProgressDialog dialog = new ProgressDialog(getActivity()    );





        @Override
        protected Long doInBackground(Void... params)
        {

            CustomerData dataHelper = new CustomerData(getActivity());

            long [] orderInfo = dataHelper.getTimeRemaining(orderId);
            Log.v(TAG,"The order "+orderId);

            orderId = orderInfo[1]; // set the order id so that what is being searched for is established
            return orderInfo[0];   // return the time

        }

        @Override
        protected void onPostExecute(Long timeRemaining)
        {

            if(timeRemaining == CustomerData.ORDER_DELIVERED)
            {

                PendingIntent sender = PendingIntent.getBroadcast(getActivity(), 0, new Intent(getActivity(),AlarmReceiver.class), PendingIntent.FLAG_UPDATE_CURRENT);

                alarmMgr.cancel(sender);
                shouldSetAlarm = false;
                timeText.setVisibility(View.INVISIBLE);
                textMins.setVisibility(View.INVISIBLE);

                textInfo.setTextSize(30);
                textInfo.setText("Your order has been delivered. Enjoy!");

                NotificationCompat.Builder mBuilder =
                        (NotificationCompat.Builder) new NotificationCompat.Builder(getActivity())
                                .setSmallIcon(com.pizzacar.tomio.pizzacar.R.drawable.honeybun)
                                .setContentTitle(getActivity().getString(com.pizzacar.tomio.pizzacar.R.string.app_name))
                                .setContentText("Your order has been delivered!");
                Intent resultIntent = new Intent(getActivity(), Home.class);

                // Because clicking the notification opens a new ("special") activity, there's
                // no need to create an artificial back stack.
                PendingIntent resultPendingIntent =
                        PendingIntent.getActivity(
                                getActivity(),
                                0,
                                resultIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT
                        );
                int mNotificationId = 32;
                mBuilder.setContentIntent(resultPendingIntent);
                NotificationManager mNotifyMgr =
                        (NotificationManager) getActivity().getSystemService(getActivity().NOTIFICATION_SERVICE);
                // Builds the notification and issues it.
                mNotifyMgr.notify(mNotificationId, mBuilder.build());

            }
            else if(timeRemaining >= 0)
            {
                timeText.setText(""+(timeRemaining/60));
                devider++;
            }

            else
            {
                PendingIntent sender = PendingIntent.getBroadcast(getActivity(), 0, new Intent(getActivity(),AlarmReceiver.class), PendingIntent.FLAG_UPDATE_CURRENT);

                if(alarmMgr != null)
                     alarmMgr.cancel(sender );

                timeText.setVisibility(View.INVISIBLE);
                textMins.setVisibility(View.INVISIBLE);

                textInfo.setTextSize(30);
                textInfo.setText("No orders placed today");

            }

        }
    }

    public void setTimeText()
    {
        GetTimeRemainingTask task = new GetTimeRemainingTask();
        task.execute() ;

    }

    public void refreshTime(Long timeRemaining)
    {
        if(timeRemaining == CustomerData.ORDER_DELIVERED)
        {

            PendingIntent sender = PendingIntent.getBroadcast(getActivity(), 0, new Intent(getActivity(),AlarmReceiver.class), PendingIntent.FLAG_UPDATE_CURRENT);

            alarmMgr.cancel(sender);
            shouldSetAlarm = false;
            timeText.setVisibility(View.INVISIBLE);
            textMins.setVisibility(View.INVISIBLE);

            if(textInfo != null)
            {
                textInfo.setTextSize(30);
                textInfo.setText("Your order has been delivered. Enjoy!");

            }
        }
        else if(timeRemaining >= 0)
        {
            if(timeText != null)
            timeText.setText(""+(timeRemaining/60));
        }

        else
        {
            PendingIntent sender = PendingIntent.getBroadcast(getActivity(), 0, new Intent(getActivity(),AlarmReceiver.class), PendingIntent.FLAG_UPDATE_CURRENT);

            if(alarmMgr != null)
                alarmMgr.cancel(sender );
            if(timeText != null)
            {
                timeText.setVisibility(View.INVISIBLE);
                textMins.setVisibility(View.INVISIBLE);

                textInfo.setTextSize(30);
                textInfo.setText("No orders placed today");
            }


        }

    }
    }

