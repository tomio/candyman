package com.pizzacar.tomio.pizzacar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.pizzacar.tomio.pizzacar.Data.CustomerData;
import com.pizzacar.tomio.pizzacar.Data.UserSharedPreferences;

public class AccountInfoActivity extends AppCompatActivity {
    private EditText userEmail;
    private EditText userAddress;
    private EditText userPayment;
    private String email;
    private String address;
    private String payment;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        setContentView(com.pizzacar.tomio.pizzacar.R.layout.activity_account_info);
        userEmail = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.info_user_email);
        userAddress = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.info_user_address);
        userPayment = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.info_user_payment);

        GetUserInfoTask task = new GetUserInfoTask();
        task.execute();

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(com.pizzacar.tomio.pizzacar.R.menu.account_info_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case com.pizzacar.tomio.pizzacar.R.id.action_edit:

                Intent intent = new Intent(this,EditAccountActivity.class);

                Bundle bundle = new Bundle();
                bundle.putString(CustomerData.CUSTOMER_EMAIL,email);
                bundle.putString(CustomerData.CUSTOMER_PAYMENT,payment);
                bundle.putString(CustomerData.CUSTOMER_ADDRESS,address);
                intent.putExtras(bundle);
                startActivity(intent);
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    public class GetUserInfoTask extends AsyncTask<Void,Void,String[]>
    {
        private ProgressDialog dialog = new ProgressDialog(AccountInfoActivity.this);


        @Override
        protected void onPreExecute()
        {
            //this.dialog.setMessage("Please wait");
            this.dialog.show();
        }

        @Override
        protected String[] doInBackground(Void... params)
        {
            CustomerData dataHelper = new CustomerData(AccountInfoActivity.this);
            return dataHelper.getAccountInfo(UserSharedPreferences.getUserEmail(AccountInfoActivity.this),
                    UserSharedPreferences.getUserPassword(AccountInfoActivity.this));



        }

        @Override
        protected void onPostExecute(String[] userInfo)
        {
            if(userInfo != null)
            {
                // the text that will be displayed to the user
                 email = userInfo[0];
                 address = userInfo[1];
                 payment = userInfo[2];

                userEmail.setText(email);
                userAddress.setText(address);
                if(!payment.equals("new"))
                    userPayment.setText(payment);
                else
                    userPayment.setHint(com.pizzacar.tomio.pizzacar.R.string.hint_no_payment);

            }

            this.dialog.dismiss();


        }
    }

}
