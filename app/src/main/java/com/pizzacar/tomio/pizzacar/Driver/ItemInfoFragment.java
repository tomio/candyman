package com.pizzacar.tomio.pizzacar.Driver;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pizzacar.tomio.pizzacar.R;


public class ItemInfoFragment extends Fragment {

    private String quantity;
    private String itemName;



    public ItemInfoFragment()
    {
        // Required empty public constructor
    }


    public void setItemInfo(String quantity,String itemName)
    {
        this.quantity = quantity;
        this.itemName = itemName;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_item_info, container, false);
        TextView textQuantity = (TextView) view.findViewById(R.id.text_item_quantity);
        TextView textItemName = (TextView) view.findViewById(R.id.text_item_name);
        textQuantity.setText("Quantity: "+quantity);
        textItemName.setText(itemName);

        return view;
    }

    


}
