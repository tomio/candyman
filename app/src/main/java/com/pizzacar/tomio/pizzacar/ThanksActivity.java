package com.pizzacar.tomio.pizzacar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ThanksActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.pizzacar.tomio.pizzacar.R.layout.activity_thanks);

        Button homeButton = (Button) findViewById(com.pizzacar.tomio.pizzacar.R.id.button_return_home);

        homeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                startActivity(new Intent(ThanksActivity.this,Home.class));

            }
        });

    }
}
