package com.pizzacar.tomio.pizzacar;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.pizzacar.tomio.pizzacar.Data.ItemData;


public class FoodItemFragment extends Fragment {

    private ImageView itemImage;
    private NumberPicker quantityPicker;
    private ImageView addButton;
    private TextView itemPrice;
    private TextView  itemName;


    public static final int ACTIVITY_MAIN = 0;
    public static final int ACTIVITY_REVIEW = 1;
    public static final String CURRENT_ACTIVITY = "current_activity";

    // info concerning the item
    private int currentActivity;
    private long id;
    private double price;
    private int addState;





    public FoodItemFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view =  inflater.inflate(com.pizzacar.tomio.pizzacar.R.layout.fragment_food_item, container, false);


        itemName = (TextView) view.findViewById(com.pizzacar.tomio.pizzacar.R.id.text_food_item_name);
        itemPrice = (TextView) view.findViewById(com.pizzacar.tomio.pizzacar.R.id.text_food_item_price);
        itemImage = (ImageView) view.findViewById(com.pizzacar.tomio.pizzacar.R.id.image_food_item);
        quantityPicker = (NumberPicker) view.findViewById(com.pizzacar.tomio.pizzacar.R.id.food_item_quantity);
        addButton = (ImageView) view.findViewById(com.pizzacar.tomio.pizzacar.R.id.image_button);

        Bundle bundle =  getArguments();

        setItemInfo(bundle);

        quantityPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {



                if(addState == ItemData.ITEM_IN_CART)
                {

                    ((OrderFragment) getParentFragment()).updateShoppingCart(id, newVal);

                }


            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleButtonImage();
            }
        });
        return view;
    }



    private void toggleButtonImage()
    {

        if(addState == ItemData.ITEM_NOT_IN_CART)
        {
            addState = ItemData.ITEM_IN_CART;
            long quantity = quantityPicker.getValue();

            if(currentActivity == ACTIVITY_MAIN)
              ((OrderFragment)getParentFragment()).addItemToShoppingCart(id, quantity);


            addButton.setImageResource(com.pizzacar.tomio.pizzacar.R.drawable.remove_button);
        }


        else
        {
            addState = ItemData.ITEM_NOT_IN_CART;

            if(currentActivity == ACTIVITY_MAIN)
                ((OrderFragment)getParentFragment()).removeItemFromShoppingCart(id);
            else
            {

            }

            addButton.setImageResource(com.pizzacar.tomio.pizzacar.R.drawable.add_button);
        }
    }

    private void setItemInfo(Bundle bundle)
    {
        price = bundle.getDouble(ItemData.ITEM_PRICE);


        itemPrice.setText("$"+price);
        itemName.setText(bundle.getString(ItemData.ITEM_NAME));
        String imageData = bundle.getString(ItemData.ITEM_IMAGE);
        setImage(imageData);

         id = bundle.getLong(ItemData.ITEM_ID);
        addState = bundle.getInt(ItemData.ITEM_CART_STATUS);
        currentActivity = bundle.getInt(CURRENT_ACTIVITY);
        quantityPicker.setMaxValue(10);
        quantityPicker.setMinValue(1);
        quantityPicker.setValue((int) bundle.getLong(ItemData.ITEM_QUANTITY));
        quantityPicker.setWrapSelectorWheel(false);



    }


    public void setImage(String imageData)
    {

        byte[] decodedImage = Base64.decode(imageData, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedImage, 0, decodedImage.length);
        itemImage.setImageBitmap(decodedByte);
    }

    public void setMargin()
    {

    }


}
