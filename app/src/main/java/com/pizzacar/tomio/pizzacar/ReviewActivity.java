package com.pizzacar.tomio.pizzacar;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.pizzacar.tomio.pizzacar.Data.CustomerData;
import com.pizzacar.tomio.pizzacar.Data.ItemData;
import com.pizzacar.tomio.pizzacar.Driver.ItemInfoFragment;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ReviewActivity extends AppCompatActivity {



    public static final String ORDER_TOTAL = "order_total";
    public static final String ORDER_ID = "order_id";

    private String[][] itemsInfo;;
    private final String TAG = ReviewActivity.class.getSimpleName();

    public static final String JUST_ORDERED = "just_ordered";

    private double orderTotal;
    private long orderId;
    private boolean hasCardSaved;
    private String deliveryTime;
    private boolean orderStillExist;
    private boolean activityCreated = false;
    private Bundle paymentBundle; // just incase the user comes back from the payment activity
    // all of the info will be saved

        //this.dialog.setMessage("Please wait");


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.pizzacar.tomio.pizzacar.R.layout.activity_review);
        Aesthetic.setBarColor(ReviewActivity.this);
        paymentBundle = new Bundle();

        Intent intent = getIntent();
        TextView lastFourText = (TextView) findViewById(com.pizzacar.tomio.pizzacar.R.id.text_last_four) ;

        TextView totalText = (TextView) findViewById(com.pizzacar.tomio.pizzacar.R.id.text_total_price);
        TextView textAddress = (TextView) findViewById(com.pizzacar.tomio.pizzacar.R.id.text_address_value);
        TextView  timeText = (TextView) findViewById(com.pizzacar.tomio.pizzacar.R.id.text_order_time_value);
        TextView promotionText = (TextView) findViewById(com.pizzacar.tomio.pizzacar.R.id.text_promotion_value);
        TextView addPromotionText = (TextView) findViewById(com.pizzacar.tomio.pizzacar.R.id.text_promotion);

        final long[] orders = intent.getLongArrayExtra(MainActivity.ORDER_INFO);
        final String orderString = intent.getStringExtra(MainActivity.ORDER_STRING);
        orderTotal = intent.getDoubleExtra(PromotionsActivity.NEW_ORDER_TOTAL,-1);
        String promotion = intent.getStringExtra(PromotionsActivity.PROMOTION_DESC);


        boolean justAddedPromotion = (promotion != null && orderTotal != -1);
        ProgressDialog dialog = new ProgressDialog(ReviewActivity.this);

        try
        {
            dialog.show();


            JSONObject json = new JSONObject(orderString);
            JSONObject orderInfo =  json.getJSONObject("order");

            if(!justAddedPromotion)
                orderTotal = orderInfo.getDouble("total");
            else
            {
                addPromotionText.setText("Promotion:");
                addPromotionText.setTextColor(Color.BLACK);
                promotionText.setText(promotion);
                promotionText.setVisibility(View.VISIBLE);
            }
            totalText.setText("$"+orderTotal);
            textAddress.setText(orderInfo.getString("location"));
            deliveryTime = json.getString("delivery_time");
            timeText.setText(deliveryTime);
            String cardJson = json.getString("card");
            Log.v(TAG,"Card json "+ cardJson );

            if(cardJson.equals("new"))
            {
                lastFourText.setVisibility(View.GONE);
                findViewById(com.pizzacar.tomio.pizzacar.R.id.text_payment).setVisibility(View.GONE);
            }
            else
            {
                JSONObject info = new JSONArray(cardJson).getJSONObject(0);
                String lastFour = info.getString("last4");
                String brand = info.getString("brand");
                lastFourText.setText(brand +" "+lastFour);
                hasCardSaved = true;
                Log.v(TAG, "The value of the string is " + String.valueOf(hasCardSaved));

            }

            orderId = json.getLong("order_id");

                promotion = json.getString("promotion");

                if(!promotion.equals("null"))
                {
                    JSONObject promotionJson = new JSONObject(promotion);


                    addPromotionText.setText("Promotion:");
                    addPromotionText.setTextColor(Color.BLACK);
                    String promotionString = promotionJson.getString("description");
                    promotionText.setText(promotionString);
                    paymentBundle.putString(PromotionsActivity.PROMOTION_DESC,promotionString);
                    promotionText.setVisibility(View.VISIBLE);
                }
                else
                {

                    addPromotionText.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(ReviewActivity.this, PromotionsActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putLong(ORDER_ID, orderId);
                            bundle.putLongArray(MainActivity.ORDER_INFO, orders);
                            bundle.putString(MainActivity.ORDER_STRING, orderString);
                            Log.v(TAG,"tHE ORDER STRING IN "+orderString) ;
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    });
                }






            JSONArray items = json.getJSONArray("items");

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            for(int i =0; i < items.length(); i++)
            {
                JSONObject item = items.getJSONObject(i);

                ItemInfoFragment frag = new ItemInfoFragment();

                frag.setItemInfo(item.getString("quantity"),item.getString("name"));

                fragmentTransaction.add(com.pizzacar.tomio.pizzacar.R.id.scroll_view_container, frag);
            }

            fragmentTransaction.commit();
            dialog.dismiss();


        }


        catch(JSONException e)
        {
            Log.e(TAG,"There was a problem getting the orderable array ",e);

        }



        TextView paymentButton = (TextView) findViewById(com.pizzacar.tomio.pizzacar.R.id.text_payment_button);
        assert paymentButton != null;
        paymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                CheckIfOrderExistTask task = new CheckIfOrderExistTask();

                task.execute(orderId);

                if(orderStillExist)
                {


                    if(hasCardSaved)
                    {
                        ConfirmOrderTask confirmTask = new ConfirmOrderTask();
                        confirmTask.execute(""+orderId);

                    }
                    else
                    {
                        Bundle bundle = new Bundle();
                        Intent intent = new Intent(ReviewActivity.this, PaymentMethodActivity.class);
                        paymentBundle.putDouble(ORDER_TOTAL, orderTotal);
                        paymentBundle.putString(ItemData.ITEM_DELIVERY_TIME, deliveryTime);
                        paymentBundle.putLong(MainActivity.ORDER_ID, orderId);
                        paymentBundle.putLongArray(MainActivity.ORDER_INFO, orders);
                        paymentBundle.putString(MainActivity.ORDER_STRING, orderString);

                        /*


        orderTotal = intent.getDoubleExtra(PromotionsActivity.NEW_ORDER_TOTAL,-1);
        String promotion = intent.getStringExtra(PromotionsActivity.PROMOTION_DESC);
                         */
                        intent.putExtras(paymentBundle);
                        startActivity(intent);
                    }

                }

                else
                {

                    new AlertDialog.Builder(ReviewActivity.this)
                            .setMessage("Order Expired. Please make another one.")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(ReviewActivity.this, Home.class));

                                }
                            })
                            .show();

                }

            }
        });


        Toolbar toolbar = (Toolbar) findViewById(com.pizzacar.tomio.pizzacar.R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }


    @Override
    public void onResume()
    {
        super.onResume();
        CheckIfOrderExistTask task = new CheckIfOrderExistTask();
        task.execute(orderId);
        Log.v(TAG,"is it " +orderStillExist+"");
        if(activityCreated && !orderStillExist)
        {
            new AlertDialog.Builder(ReviewActivity.this)
                    .setMessage("Order Expired. Please make another one.")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            startActivity(new Intent(ReviewActivity.this, Home.class));

                        }
                    })
                    .show();

        }
        else
            activityCreated = true;

    }




    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            DeleteOrderTask task = new DeleteOrderTask();
            task.execute(orderId);
            return false;
        }


        return super.onOptionsItemSelected(menuItem);
    }


    public class DeleteOrderTask extends AsyncTask<Long,Void,Void>
    {
        @Override
        protected Void doInBackground(Long... params)
        {

            CustomerData dataHelper = new CustomerData(ReviewActivity.this);
            dataHelper.deletePendingOrder(params[0]);
            return null ;
        }
    }

        public class ConfirmOrderTask extends AsyncTask<String,Void,String>
    {
        private ProgressDialog dialog = new ProgressDialog(ReviewActivity.this);


        @Override
        protected void onPreExecute()
        {
            //this.dialog.setMessage("Please wait");
            this.dialog.show();
        }




        @Override
        protected String doInBackground(String... params)
        {

            ItemData dataHelper = new ItemData(ReviewActivity.this);
            return dataHelper.confirmOrder(params[0]);

        }

        @Override
        protected void onPostExecute(String confirmedString)
        {

            Log.v(TAG, "The confirmed string is " + confirmedString);

            try
            {
                JSONObject json = new JSONObject(confirmedString);
                if(json.getBoolean("success"))
                {
                    Intent intent = new Intent(ReviewActivity.this, ThanksActivity.class);
                    Bundle bundle  = new Bundle();
                    bundle.putBoolean(JUST_ORDERED,true);
                    intent.putExtras(bundle);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    this.dialog.dismiss();
                    startActivity(intent);
                }

                else
                {
                    new AlertDialog.Builder(ReviewActivity.this)
                            .setMessage(json.getString("message")+" Please try again or re-enter card info in account settings .")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    startActivity(new Intent(ReviewActivity.this, AccountInfoActivity.class));

                                }
                            })
                            .show();
                }


            }
            catch (JSONException e)
            {

            }


        }
    }

    public class CheckIfOrderExistTask extends AsyncTask<Long,Void,Boolean>
    {
        @Override
        protected Boolean doInBackground(Long... params)
        {
            CustomerData dataHelper = new CustomerData(ReviewActivity.this);
            return dataHelper.checkOrderStillExist(params[0]);

        }

        @Override
        protected void onPostExecute(Boolean stillExist)
        {
            orderStillExist = stillExist;
        }


    }
}


