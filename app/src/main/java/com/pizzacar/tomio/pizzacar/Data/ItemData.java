package com.pizzacar.tomio.pizzacar.Data;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by tomio on 5/5/2016.
 */


public class ItemData
{



    public static final String ITEM_PRICE = "price";
    public static final String ITEM_NAME = "name";
    public static final String ITEM_IMAGE = "image";
    public static final String ITEM_ID = "id";
    public static final int ITEM_IN_CART = 1;
    public static final int ITEM_NOT_IN_CART = 0;
    public static final String ITEM_CART_STATUS = "cart_status";
    public static final String ITEM_QUANTITY = "quantity";
    public static final String ITEM_DELIVERY_TIME = "delivery_time";

    public static final int POSITION_ITEM_ID = 0;
    public static final int POSITION_ITEM_NAME = 1;
    public static final int POSITION_ITEM_IMAGE = 2;
    public static final int POSITION_ITEM_PRICE = 3;
    public static final int LENGTH_INFO_ARRAY = 4;

    private  static final String TAG = ItemData.class.getSimpleName();
    private Context context;
    public  ItemData(Context c)
    {
         context = c;
    }
    private String message;
    private boolean canMakeOrder;



    public String[][] getOrderables()
    {

        Request requestHelper = new Request(context);
        String[][] orderableInfoArray = null;

        String jsonString = requestHelper.makeRequest("orderables", "GET", "");

        try
        {
            orderableInfoArray = extractOrderableInfo(jsonString);
        }
        catch (JSONException e)
        {
            Log.e(TAG, "There was a probmlem extracting from the json String " + e.getMessage(), e);
            e.printStackTrace();
        }




        return orderableInfoArray;
    }




    public String[][] extractOrderableInfo(String jsonString)
            throws JSONException
    {
        JSONArray orderableArray= new JSONArray(jsonString);

        int hour = orderableArray.getJSONObject(0).getInt("time");
        this.canMakeOrder = checkCanMakeOrder(hour);

        String[][] orderableInfoArray = new String[orderableArray.length() -1][LENGTH_INFO_ARRAY];
        for(int i = 1; i < orderableArray.length(); i++)
        {
            JSONObject orderableInfo = orderableArray.getJSONObject(i);
            String name = orderableInfo.getString("name");
            String price = orderableInfo.getString("price");
            String id = orderableInfo.getString("id");
            String imageData = orderableInfo.getString("image");
            String[] orderable = orderableInfoArray[i-1]; // so that the array of elemets start on zero 

            Log.v(TAG,"The price is "+price);
            orderable[POSITION_ITEM_ID] = id;
            orderable[POSITION_ITEM_PRICE] = price;
            orderable[POSITION_ITEM_NAME] = name;
            orderable[POSITION_ITEM_IMAGE] = imageData;
        }

        Log.v(TAG,"The orderable array length "+orderableInfoArray.length);



        return orderableInfoArray;

    }

    public boolean canMakeOrder()
    {
        return this.canMakeOrder;
    }
    public boolean checkCanMakeOrder(int hour)
    {
        if( hour < 18 || hour > 21)
            return false;
        else
            return true;

    }

    public String submitInitialOrder( String orderInfo)
    {

        long userId = UserSharedPreferences.getUserId(context);
        String jsonString;
        Request requestHelper = new Request(context);
        String params = "order_info="+orderInfo+"&user_id="+userId;


        jsonString = requestHelper.makeRequest("new_orders/new_order", "POST",params);

        try
        {
            long driverId = extractDriverId(jsonString);
            if(driverId < 0)
                jsonString = null;
        }
        catch (JSONException e)
        {
            Log.e(TAG,"Could not extract driver id ",e);
        }

        Log.v(TAG,params);
        return jsonString;

    }


    private long extractDriverId(String jsonString)
            throws JSONException
    {

        JSONObject json = new JSONObject(jsonString);
        long driverId =json.getLong("driver_id");

        if(driverId < 0)
           this.message =  json.getString("message");

        return driverId;

    }

    public String getErrorMessage()
    {
        return this.message;
    }
    public String confirmOrder(String tokenId,String orderId)
    {

        Request requestHelper = new Request(context);
        String params = "stripeToken="+tokenId;
        Log.v(TAG,"THE TOKEN iD "+tokenId);
        String jsonString = requestHelper.makeRequest("confirm/"+orderId,"POST",params);

        return jsonString;
    }

    public String confirmOrder(String orderId)
    {

        Request requestHelper = new Request(context);
        String jsonString = requestHelper.makeRequest("confirm/"+orderId,"POST","");

        return jsonString;
    }







}
