package com.pizzacar.tomio.pizzacar.Data;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by tomio on 5/31/2016.
 */


public class CustomerData
{

    public static final String CUSTOMER_ADDRESS = "address";
    public static final String CUSTOMER_EMAIL = "email";
    public static final String CUSTOMER_PASSWORD = "password";
    public static final String CUSTOMER_PAYMENT = "payment";
    public static final String CUSTOMER_TIME_REMAINING = "time";

    public static final long ORDER_DELIVERED = 121324;

    private String message;
    private boolean addressIsValid;

    private final String TAG = CustomerData.class.getSimpleName();


    Context context;
    public CustomerData(Context c)
    {
        context = c;
        

    }
    public long createCustomer(String email,String password)
    {

        long userId = -1;
        Request request = new Request(context);
        String params = "user[email]="+email + "&user[password]="+password;
        String response = request.makeRequest("create_user","POST",params);
        try
        {
            userId = extractUserId(response);
            if(userId == -1)
            {
                this.message = extractSignUpErrorMessage(response);
               Log.v(TAG,"Error"+message);
            }
        }

        catch (JSONException e)
        {
            Log.e(TAG,"There was a problem extracting the user id ",e  );

        }
        Log.v(TAG,"The created user respone is "+response);

        return userId;
    }

    public String updateAddress(String address)
    {
        Request request = new Request(context);
        String response =  request.makeRequest("update_address","POST","address="+address);
        String validity = "FOUND";
        if (request.getStatusCode() != 200)
        {
            try
            {

                JSONObject json = new JSONObject(response);

                this.message =json.getString("message");
                validity = json.getString("address_status");



                Log.v(TAG,"The message is" +this.message);

            }
            catch(JSONException e )
            {
                Log.e(TAG,"There was a problem extracting the sign up message",e);
            }
        }
        return validity;
    }

    public void deletePendingOrder(long orderId)
    {
        Request request = new Request(context);
        request.makeRequest("delete_pending_order", "POST", "id=" + orderId);
    }
    public boolean checkOrderStillExist(long orderId)
    {
        Request request = new Request(context);
        String response = request.makeRequest("order_still_exist","GET","id="+orderId);
        boolean exists = false;
        try
        {
            JSONObject json = new JSONObject(response);
            exists = json.getBoolean("exist");

        }
        catch(JSONException e )
        {
            Log.e(TAG,"There was a problem checking existence ",e);
        }
        return exists;
    }


    public String addPromo(String code,long orderId)
    {
        Request request = new Request(context);
        return request.makeRequest("add_promotion","POST","order_id="+orderId+"&code="+code);

    }
    public boolean orderExists()
    {
        Request request = new Request(context);
        String jsonString = request.makeRequest("order_exists","GET","");
        boolean exists = false;
        try
        {
            JSONObject json = new JSONObject(jsonString);
            exists = json.getBoolean("exist");

        }
        catch(JSONException e)
        {
            Log.e(TAG,"There was a problem telling wheather the order exists ",e);

        }

        return exists;
    }

    // get the time remaining for an order to be delivered
    public long[] getTimeRemaining(long orderId)
    {
        Log.v(TAG,"The order id passed is "+orderId);
        Request request = new Request(context);
        String timeJson;
        if(orderId > 0)
        {
            Log.v(TAG,"the order id was greater than 0");
            timeJson = request.makeRequest("remaining_time", "GET", "order_id=" + orderId);
        }
        else
              timeJson = request.makeRequest("remaining_time","GET","");

        long time = -1;

        try
        {
             JSONObject json =  new JSONObject(timeJson);
             time = json.getLong("time");
             orderId = json.getLong("order_id");

             if(time > 0 && json.getInt("state") == 2)
             {
                time = ORDER_DELIVERED;
             }

        }
        catch(JSONException e)
        {
            Log.e(TAG,"There was a problem extracting the time",e);

        }

        return new long[]{time,orderId};

    }

    public void updateGCMId(String token)
    {
        Request request = new Request(context);
        Log.v(TAG,"the token passed "+token);
        request.makeRequest("add_gcm_id","POST","gcm_id="+token);
    }
    public long login(String email, String password)
    {


        long userId  = -1;
        Request request = new Request(context);
        String params = "email="+email + "&password="+password;
        Log.v(TAG,"THE PARAMS ARE "+params);
        String response = request.makeRequest("login","POST",params);
        try
        {
            JSONObject json = new JSONObject(response);
            userId =  json.getLong("id");

            if(userId > 0 && userId != 3)
            {
                if(!json.getString("address").equals("null"))
                    UserSharedPreferences.setAddressIsValid(context,true);

                if(json.getBoolean("tester"))
                {
                    UserSharedPreferences.setIsTester(context,true);
                }

            }

        }

        catch (JSONException e)
        {
            Log.e(TAG,"There was a problem extracting the user id ",e  );
        }
        Log.v(TAG,"The created user respone is "+response);





        return userId;

    }

    public String[] getAccountInfo(String email, String password)
    {
        String[] userInfo = null;

        Request request = new Request(context);
        String params = "email="+email + "&password="+password;
        Log.v(TAG,"THE PARAMS ARE "+params);
        String response = request.makeRequest("account","POST",params);

        try
        {
            userInfo = extractUserInfo(response);
        }
        catch (JSONException e)
        {
            Log.e(TAG,"There was a problem extracting user info "+e.getMessage());
        }

        return userInfo;


    }

    public boolean updateUserInfo(String newEmail, String address)
    {
        Request request = new Request(context);
        String jsonString = request.update("users","address="+address+"&new_email="+newEmail);

        if(request.getStatusCode() == 200)
        {
            return true;
        }
        else
        {
            try
            {

                this.message = extractUpdateErrorMessage(jsonString);

            }
            catch (JSONException e)
            {
                Log.e(TAG,"Could not extract the update error message",e);
            }
            return false;
        }


    }

    public String updatePaymentMethod(String email, String password,String stripeToken)
    {
        Request request = new Request(context);
        String params = "email="+email+"&password="+password+"&stripe_token="+stripeToken;
        String cardInfo = request.update("update_payment",params);
        return cardInfo;
    }

    private String extractValidity(String jsonString)
            throws JSONException
    {
        return  new JSONObject(jsonString).getString("message");
    }
    private long extractUserId(String jsonString)
            throws JSONException
    {
        JSONObject json = new JSONObject(jsonString);
        return json.getJSONObject("user").getLong("id");

    }

    private String extractSignUpErrorMessage(String jsonString)
            throws JSONException
    {
        JSONObject json = new JSONObject(jsonString);
        return json.getString("message");

    }

    private String extractUpdateErrorMessage(String jsonString )
            throws JSONException
    {
        JSONArray messages = new JSONObject(jsonString).getJSONArray("message");
        String message = "";
        for(int i = 0; i< messages.length(); i++)
        {
            message+= messages.getString(0);
        }

        return message;
    }

    public String getSignUpErrorMessage()
    {
        return this.message;
    }
    private String[] extractUserInfo(String jsonString)
       throws JSONException
    {
        JSONObject json = new JSONObject(jsonString);
        String email = json.getString("email");
        String address = json.getString("address");
        String cardJson = json.getString("card");
        String cardText;
        if(!cardJson.equals("new"))
        {
            JSONObject cardInfo = new JSONArray(cardJson).getJSONObject(0);
            cardText = cardInfo.getString("brand") +" **** " + cardInfo.getString("last4");
            
        }

        else
        {
            cardText = "new";
        }

        return new String[]{email,address,cardText};



    }







}
