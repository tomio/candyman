package com.pizzacar.tomio.pizzacar;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pizzacar.tomio.pizzacar.Data.ItemData;
import com.pizzacar.tomio.pizzacar.Data.UserSharedPreferences;
import com.stripe.android.*;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.AuthenticationException;

import org.json.JSONException;
import org.json.JSONObject;

public class PaymentMethodActivity extends AppCompatActivity {
    private static final String TAG = PaymentMethodActivity.class.getSimpleName();

    private TextView confirmOrderText;
    private EditText cardNumberField;
    private EditText cardSecurityField;
    private EditText cardExpirationMonthField;
    private EditText cardExpirationYearField;

    private TextView totalText;
    private TextView timeText;

    private long orderId;
    private String orderString;
    private long[] orders;
    private double total;
    private String promotionDesc;

    private static final String PUBLISHABLE_LIVE_KEY = "pk_live_BZsprNGlg7Jnkv7eFPNxF6Lj";
    private static final String PUBLISHABLE_TEST_KEY = "pk_test_8gZ3iToQqY6bJ22olONcPHVl";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.pizzacar.tomio.pizzacar.R.layout.activity_payment_method);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        confirmOrderText = (TextView) findViewById(com.pizzacar.tomio.pizzacar.R.id.text_confirm_order);
        cardNumberField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.edit_card_number);
        cardExpirationMonthField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.edit_card_expiration_month);
        cardExpirationYearField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.edit_card_expiration_year);
        cardSecurityField = (EditText) findViewById(com.pizzacar.tomio.pizzacar.R.id.edit_card_security);

        totalText = (TextView) findViewById(com.pizzacar.tomio.pizzacar.R.id.text_order_total_payment);
        cardNumberField.addTextChangedListener(new TextWatcher() {

            private static final char space = ' ';

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {  }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {


                // Remove spacing char
                if (s.length() > 0 && (s.length() % 5) == 0) {
                    final char c = s.charAt(s.length() - 1);
                    if (space == c) {
                        s.delete(s.length() - 1, s.length());
                    }
                }
                // Insert char where needed.
                if (s.length() > 0 && (s.length() % 5) == 0) {
                    Log.v(TAG, "I was called ");
                    char c = s.charAt(s.length() - 1);
                    // Only if its a digit where there should be a space we insert a space
                    if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3)
                    {

                        s.insert(s.length() - 1, String.valueOf(space));

                    }
                }
            }
        });


        timeText = (TextView) findViewById(com.pizzacar.tomio.pizzacar.R.id.text_order_time_payment);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
        {
            orderId = bundle.getLong(MainActivity.ORDER_ID);
            total = bundle.getDouble(ReviewActivity.ORDER_TOTAL);
            totalText.setText("Order Total: $" + total);
            timeText.setText("Estimated Delivery Time:  " + bundle.getString(ItemData.ITEM_DELIVERY_TIME));
            orderString = bundle.getString(MainActivity.ORDER_STRING);
            orders = bundle.getLongArray(MainActivity.ORDER_INFO);
            promotionDesc = bundle.getString(PromotionsActivity.PROMOTION_DESC);




        confirmOrderText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String cardNumber = cardNumberField.getText().toString().trim();
                String cardSecurity = cardSecurityField.getText().toString().trim();
                String year = cardExpirationYearField.getText().toString().trim();
                String month = cardExpirationMonthField.getText().toString().trim();


                if(cardNumber.equals("") || cardSecurity.equals("") || year.equals("") || month.equals(""))
                {
                    Toast.makeText(PaymentMethodActivity.this,"Please fill out all of the fields",Toast.LENGTH_LONG).show();
                }

                else
                {
                    int expirationYear = Integer.parseInt(year);
                    int expirationMonth = Integer.parseInt(month);

                    Card card = new Card(
                            cardNumber,
                            expirationMonth,
                            expirationYear,
                            cardSecurity
                    );


                    if (card.validateCard()) {
                        Log.v(TAG,"Got here ssss");
                        try {

                            String key;
                            if(UserSharedPreferences.checkIsTester(PaymentMethodActivity.this))
                                key = PUBLISHABLE_TEST_KEY;
                            else
                                key = PUBLISHABLE_LIVE_KEY;

                            Stripe stripe = new Stripe(key);


                            stripe.createToken(
                                    card,
                                    new TokenCallback() {
                                        public void onSuccess(Token token) {

                                            // Send token to your server
                                            ConfirmOrderTask task = new ConfirmOrderTask();
                                            task.execute(token.getId(), orderId + "");


                                        }

                                        public void onError(Exception error) {
                                            // Show localized error message

                                            Toast.makeText(PaymentMethodActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                                            Log.e(TAG,"There was a problem with the card",error);
                                        }
                                    }
                            );


                        } catch (AuthenticationException e) {

                        }


                    }
                    else
                    {
                        new AlertDialog.Builder(PaymentMethodActivity.this)
                                .setMessage("The card you entered was invalid. Please try again")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // startActivity(new Intent(PaymentMethodActivity.this, U.class));

                                    }
                                })
                                .show();


                    }
                }




            }
        });

    }
        Aesthetic.setBarColor(PaymentMethodActivity.this);


    }




      public class ConfirmOrderTask extends AsyncTask<String,Void,String> {

        private ProgressDialog dialog = new ProgressDialog(PaymentMethodActivity.this);


        @Override
        protected void onPreExecute() {
            //this.dialog.setMessage("Please wait");
            this.dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {

            ItemData dataHelper = new ItemData(PaymentMethodActivity.this);
            return dataHelper.confirmOrder(params[0], params[1]);

        }

        @Override
        protected void onPostExecute(String confirmedString) {

            Log.v(TAG, "The confirmed string is " + confirmedString);
            try {

                this.dialog.dismiss();

                JSONObject json = new JSONObject(confirmedString);
                if (json.getBoolean("success")) {
                    startActivity(new Intent(PaymentMethodActivity.this, ThanksActivity.class));

                } else {
                    new AlertDialog.Builder(PaymentMethodActivity.this)
                            .setMessage(json.getString("message") + " Please re-enter card info or try another card.")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // startActivity(new Intent(PaymentMethodActivity.this, U.class));

                                }
                            })
                            .show();

                }
            } catch (JSONException e) {
                Log.e(TAG, "There was a problem getting the card json", e);
            }


        }





    }

}
