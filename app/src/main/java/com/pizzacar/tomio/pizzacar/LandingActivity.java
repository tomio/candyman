package com.pizzacar.tomio.pizzacar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.pizzacar.tomio.pizzacar.Data.UserSharedPreferences;
import com.pizzacar.tomio.pizzacar.Driver.DriverHome;

public class LandingActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.pizzacar.tomio.pizzacar.R.layout.activity_landing);

        long userId = UserSharedPreferences.getUserId(LandingActivity.this);
        if (userId> 0)
        {

            if(userId == 3)
            {
                Intent intent = new Intent(LandingActivity.this, DriverHome.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
            else
            {
                Intent intent = new Intent(LandingActivity.this,Home.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }

        }


        findViewById(com.pizzacar.tomio.pizzacar.R.id.button_landing_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LandingActivity.this, SignInActivity.class));
            }
        });


        findViewById(com.pizzacar.tomio.pizzacar.R.id.button_landing_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LandingActivity.this,ActivitySignUp.class));
            }
        });




    }
}
